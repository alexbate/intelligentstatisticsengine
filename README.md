# An Intelligent Statistics Engine

This was a project created as a Part II Project for the University of Cambridge's Computer Science Tripos. It explores data warehousing, analysis and machine learning. A more through description and discussion can be found in diss.pdf. As a dissertation project, precedence was given to producing enough interesting things to discuss over production-quality code. 

## Overview of modules
* `loader` - processes a series of discrete log events, maintaining a separate database containing summarized information
* `statistics` - reads the database made in `loader`, presents information in an API and gives each one an 'interesting score'.
* `feedback tool`  - used for the user study to quickly provide training data
* `user study tool` - used in the user study to provide a basic interface to the `statistics` module.

## Installation
Make sure you have a new PostgreSQL database for this project to use - it needs to support UTF8 characters. Then, see README files in each module for specific installation instructions.

## Usage
See README files in each module. A full API reference for the statistics module is in the appendices of the dissertation (diss.pdf).