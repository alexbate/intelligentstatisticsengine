## Loader

### Installation guide
1. Fill out `.credentials` file with database credentials
2. Fill out `Configuration.java` with the required settings
3. `./gradlew loader:run`

### Overview
This module receives new JSON log events, initially from another database and later via a HTTP endpoint*, and uses them to maintain its own database containing summarised information from them. Where necessary, it fetches metadata about objects referred to in the log events.

*In hindsight, it may have been better to continue to fetch them from the main database.
