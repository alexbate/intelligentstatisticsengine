package uk.co.alexbate.IntelligentStatisticsEngine.loader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata.UserGetter;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.parser.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * Created by alex on 09/11/15.
 */
public class BatchImporter {
    private static HashMap<String, EventParser> stringToParserMap = new HashMap<String, EventParser>();
    private static VideoParser videoParser = new VideoParser();
    private static ViewQuestionParser viewQuestionParser = new ViewQuestionParser();
    private static AnswerQuestionParser answerQuestionParser = new AnswerQuestionParser();
    private static ShareParser shareParser = new ShareParser();
    private static HintParser hintParser = new HintParser();

    static DateFormat df;

    static {
        stringToParserMap.put("VIDEO_PLAY", videoParser);
        stringToParserMap.put("VIDEO_PAUSE", videoParser);
        stringToParserMap.put("VIEW_QUESTION", viewQuestionParser);
        stringToParserMap.put("ANSWER_QUESTION", answerQuestionParser);
        stringToParserMap.put("USE_SHARE_LINK", shareParser);
        stringToParserMap.put("VIEW_HINT", hintParser);
    }

    static {
       df = new SimpleDateFormat("yyyy-mm-dd");
    }

    private static void thingsToDoForEveryRecord(JSONObject json) throws JSONException, SQLException{
        new UserGetter(json.getString("userId")).createUserRowIfNecessary();

        String SQL = "UPDATE user_table SET last_seen = ? WHERE user_id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        try {
            statement.setDate(1, new Date(df.parse(json.getString("timestamp")).getTime()));
        } catch (ParseException e) {
            System.err.println(json.getString("timestamp"));
            e.printStackTrace();
        }
        statement.setString(2, json.getString("userId"));
    }

    public static void parseEventsFromDB() {
        long events = 0;
        long success = 0;

        try {
            String SQL = "SELECT * FROM logged_events";

            IsaacDatabaseInitializer.getDatabaseConnection().setAutoCommit(false);
            Statement statement = IsaacDatabaseInitializer.getDatabaseConnection().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(50);

            ResultSet results = statement.executeQuery(SQL);

            while (results.next()) {
                JSONObject json = new JSONObject();

                json.put("id", results.getInt("id"));
                json.put("userId", results.getString("user_id"));
                json.put("anonymousUser", results.getBoolean("anonymous_user"));
                json.put("eventType", results.getString("event_type"));
                json.put("eventDetailsType", results.getString("event_details_type"));
                String detailsStr = results.getString("event_details");
                JSONObject details;
                try {
                    details = new JSONObject(detailsStr);
                } catch (JSONException | NullPointerException e) {
                    details = new JSONObject();
                }
                json.put("eventDetails", details);
                json.put("ipAddress", results.getString("ip_address"));
                json.put("timestamp", results.getDate("timestamp"));

                if (parseAndProcessSingleEvent(json)) {
                    success++;
                }

                events++;

                if (events % 100 == 0) {
                    System.out.println(success + "/" + events + " events processed");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static boolean parseAndProcessSingleEvent(JSONObject json) {
        String eventType = "";
        try {
            eventType = json.getString("eventType");
        } catch (JSONException e) {
            return false;
        }

        try {
            thingsToDoForEveryRecord(json);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (stringToParserMap.containsKey(eventType)) {
            return stringToParserMap.get(eventType).parseEvent(json);
        } else {
            // Not parseable
            return false;
        }
    }

    public static void parseAndProcessMultipleEvents(JSONArray array) {
        for (int i=0; i<array.length(); i++) {
            try {
                parseAndProcessSingleEvent(array.getJSONObject(i));
            } catch (JSONException e) {
                System.err.println("JSONException" + e.getStackTrace()[0]);
            }
        }
    }

    public static void parseAndProcessMultipleEventsFromMongoexport(String filepath) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filepath));

            while (reader.ready()) {
                parseAndProcessSingleEvent(new JSONObject(reader.readLine()));
            }
        } catch (java.io.IOException e) {
            System.err.println("File " + filepath + " not found");
        } catch (JSONException e) {
            System.err.println("Error parsing JSON");
        }
    }
}
