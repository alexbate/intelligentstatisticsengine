package uk.co.alexbate.IntelligentStatisticsEngine.loader;

/**
 * Created by alex on 17/01/16.
 */
public class Configuration {

    public static String MAIN_ISAAC_DATABASE_NAME = "main_db_name";
    public static final String MAIN_ISAAC_DB_URL = "localhost:portno";
    public static final String GOOGLE_SHORT_URL_API_KEY = "https://www.googleapis.com/urlshortener/v1/url?key=<key>&shortUrl=";
    public static final String ISAAC_QUESTION_API_PATH = "https://isaacphysics.org/api/pages/questions";
    public static String DATABASE_NAME = "stats_db_name";
    public static final String DB_URL = "localhost:portno";
    public static final String KEYSTORE_PATH = "path/to/keystore";
    public static final String KEYSTORE_PASSWORD = "keystorepass";
    public static final String TRUSTSTORE_PATH = "path/to/truststore";
    public static final String TRUSTSTORE_PASSWORD = "truststorepass";
}
