package uk.co.alexbate.IntelligentStatisticsEngine.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;

/**
 * Created by alex on 08/11/15.
 */
public class DatabaseInitializer {
    private static Connection databaseConnection;

    public static void clearDBConnection() {
        try {
            databaseConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            //Do nothing - we're going to set it to null anyway!
        }
        databaseConnection = null;
    }

    public static Connection getDatabaseConnection() {
        try {
            if (databaseConnection == null || databaseConnection.isClosed()) {
                openDatabaseConnection();
                createTablesIfNecessary(getTableSchemas());
            }
        } catch (SQLException e) {
            System.err.println("Error checking if database is closed");
            System.exit(3);
        }

        return databaseConnection;
    }

    private static void createTablesIfNecessary(LinkedList<String> tableSchemas) {
        for(String sql : tableSchemas) {
            try {
                Statement statement = databaseConnection.createStatement();
                statement.execute(sql);
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void openDatabaseConnection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println("Database driver not found - fatal");
            System.exit(1);
        }

        // These credentials work for Travis CI, but can be overriden with a .credentials file
        String username = "postgres";
        String password = "";

        try {
            BufferedReader credentialReader = new BufferedReader(new FileReader(".credentials"));
            username = credentialReader.readLine();
            password = credentialReader.readLine();
        } catch (FileNotFoundException e) {
            // Use the defaults, all is well
        } catch (IOException e) {
            System.err.println("Error reading credentials file");
            System.exit(2);
        }

        try {
            databaseConnection = DriverManager.getConnection("jdbc:postgresql:" + Configuration.DATABASE_NAME + "//" + Configuration.DB_URL, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static LinkedList<String> getTableSchemas() {
        final LinkedList<String> sqlStatements = new LinkedList<String>();
        final String questionTable = "CREATE TABLE IF NOT EXISTS question\n" +
                "(\n" +
                "  id text NOT NULL,\n" +
                "  views integer,\n" +
                "  num_correct_answers integer,\n" +
                "  num_incorrect_answers integer,\n" +
                "  PRIMARY KEY (id)\n" +
                ")";
        sqlStatements.add(questionTable);

        final String questionTagTable = "CREATE TABLE IF NOT EXISTS question_tag\n" +
                "(\n" +
                "  question_id text,\n" +
                "  tag_id text, \n" +
                "  PRIMARY KEY (question_id, tag_id) \n" +
                ")";
        sqlStatements.add(questionTagTable);

        final String sharesTable = "CREATE TABLE IF NOT EXISTS shares\n" +
                "(\n" +
                "  link text PRIMARY KEY,\n" +
                "  page text,\n" +
                "  frequency integer\n" +
                ")";
        sqlStatements.add(sharesTable);

        final String userQuestionTable = "CREATE TABLE IF NOT EXISTS user_question\n" +
                "(\n" +
                "  user_id text NOT NULL,\n" +
                "  question_id text NOT NULL,\n" +
                "  num_correct integer,\n" +
                "  num_incorrect integer,\n" +
                "  num_views integer,\n" +
                "  hints_used integer, \n" +
                "  PRIMARY KEY (user_id, question_id)\n" +
                ")";
        sqlStatements.add(userQuestionTable);

        final String userTable = "CREATE TABLE IF NOT EXISTS user_table\n" +
                "(\n" +
                "  user_id text NOT NULL,\n" +
                "  gender character(20),\n" +
                "  role text,\n" +
                "  school_id integer,\n" +
                "  last_seen timestamp without time zone,\n" +
                "  questions_correct integer,\n" +
                "  CONSTRAINT user_table_pkey PRIMARY KEY (user_id)\n" +
                ")";
        sqlStatements.add(userTable);

        final String videoTable = "CREATE TABLE IF NOT EXISTS video_event\n" +
                "(\n" +
                "  video_id text NOT NULL,\n" +
                "  time_in_seconds integer,\n" +
                "  pause_frequency integer,\n" +
                "  play_frequency integer,\n" +
                "  PRIMARY KEY (video_id)\n" +
                ")";
        sqlStatements.add(videoTable);

        final String questionAnswerTable = "CREATE TABLE IF NOT EXISTS question_answer\n" +
                "(\n" +
                "  question_id text NOT NULL,\n" +
                "  answer text NOT NULL,\n" +
                "  frequency integer,\n" +
                "  PRIMARY KEY(question_id, answer)\n" +
                ")";
        sqlStatements.add(questionAnswerTable);

        return sqlStatements;
    }
}
