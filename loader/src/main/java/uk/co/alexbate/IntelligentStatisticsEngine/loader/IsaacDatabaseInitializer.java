package uk.co.alexbate.IntelligentStatisticsEngine.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by alex on 08/11/15.
 */
public class IsaacDatabaseInitializer {
    private static Connection databaseConnection;

    public static Connection getDatabaseConnection() {
        try {
            if (databaseConnection == null || databaseConnection.isClosed()) {
                openDatabaseConnection();
            }
        } catch (SQLException e) {
            System.err.println("Error checking if database is closed");
            System.exit(3);
        }

        return databaseConnection;
    }

    private static void openDatabaseConnection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println("Database driver not found - fatal");
            System.exit(1);
        }

        // These credentials work for Travis CI, but can be overriden with a .credentials file
        String username = "postgres";
        String password = "";

        try {
            BufferedReader credentialReader = new BufferedReader(new FileReader(".credentials"));
            username = credentialReader.readLine();
            password = credentialReader.readLine();
        } catch (FileNotFoundException e) {
            // Use the defaults, all is well
        } catch (IOException e) {
            System.err.println("Error reading credentials file");
            System.exit(2);
        }

        try {
            databaseConnection = DriverManager.getConnection("jdbc:postgresql:" + Configuration.MAIN_ISAAC_DATABASE_NAME + "//" + Configuration.MAIN_ISAAC_DB_URL, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
