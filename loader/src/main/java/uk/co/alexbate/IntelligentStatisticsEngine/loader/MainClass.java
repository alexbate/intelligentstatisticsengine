package uk.co.alexbate.IntelligentStatisticsEngine.loader;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import static spark.Spark.halt;
import static spark.Spark.post;
import static spark.SparkBase.secure;

/**
 * Created by alex on 08/11/15.
 */
public class MainClass {

    static ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();

    public static void main(String[] args) {
        DatabaseInitializer.getDatabaseConnection();

        System.out.println(new Date().toString());
        BatchImporter.parseEventsFromDB();
//        BatchImporter.parseAndProcessMultipleEventsFromMongoexport("/tmp/mongodump");
        System.out.println(new Date().toString());
        if (Files.exists(Paths.get(Configuration.KEYSTORE_PATH))) {
            if(Files.exists(Paths.get(Configuration.TRUSTSTORE_PATH))) {
                secure(Configuration.KEYSTORE_PATH, Configuration.KEYSTORE_PASSWORD, Configuration.TRUSTSTORE_PATH, Configuration.TRUSTSTORE_PASSWORD);
            }
        }
        post("/logEvent", (request, response) -> {
            queue.add(request.body());
            halt(200);
            return request.body();
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Starting consumer thread");
                while (true) {
                    while(queue.isEmpty()) {}

                    try {
                        JSONObject json = new JSONObject(queue.poll());
                        BatchImporter.parseAndProcessSingleEvent(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
