package uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.Configuration;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 15/11/15.
 */
public class QuestionGetter {
    final String questionId;

    public QuestionGetter(String id) {
        questionId = id;
    }

    public static String normaliseQuestionID(String input) {
        int barLocation = input.indexOf("|");
        return barLocation != -1 ? input.substring(0, barLocation) : input;
    }

    public void getQuestionAndCreateRow() {
        String checkSQL = "SELECT 1 FROM question WHERE id= ?";
        ResultSet checkResults;
        try {
            PreparedStatement checkStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(checkSQL);
            checkStatement.setString(1, questionId);
            checkResults = checkStatement.executeQuery();
            if (checkResults.isBeforeFirst()) { //there is a result
                return;
            } else {
                OkHttpClient client = new OkHttpClient();
                final String questionUrl = Configuration.ISAAC_QUESTION_API_PATH + "/" + questionId;
                Request questionRequest = new Request.Builder().url(questionUrl).build();
                Response response = client.newCall(questionRequest).execute();

                JSONObject questionJson = new JSONObject(response.body().string());
                JSONArray tags = null;
                if (questionJson.has("tags")) {
                    tags = questionJson.getJSONArray("tags");
                }

                String insertSQL = "INSERT INTO question (id, views, num_correct_answers, num_incorrect_answers) " +
                        "VALUES (?, 0, 0, 0);";
                PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);
                statement.setString(1, questionId);
                statement.execute();

                String tagsSQL = "INSERT INTO question_tag (question_id, tag_id) VALUES (?, ?);";

                statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(tagsSQL);
                statement.setString(1, questionId);


                if (tags != null) {
                    for (int tagNo = 0; tagNo < tags.length(); tagNo++) {
                        String tagName = tags.getString(tagNo);
                        statement.setString(2, tagName);
                        statement.addBatch();
                    }
                }
                statement.executeBatch();

            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        } catch (JSONException e) {
            System.err.println(questionId);
            e.printStackTrace(System.err);
        }
    }
}
