package uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata;

import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.IsaacDatabaseInitializer;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 23/11/15.
 */
public class UserGetter {
    final String userID;

    public UserGetter(String userID) {
        this.userID = userID;
    }

    public void createUserRowIfNecessary() {
        String checkSQL = "SELECT 1 FROM user_table WHERE user_id= ?";
        String checkSQL1 = "SELECT * FROM users WHERE id=? OR _id=?";

        try {
            PreparedStatement preparedStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(checkSQL);
            preparedStatement.setString(1, userID);

            ResultSet checkResults = preparedStatement.executeQuery();

            if (checkResults.isBeforeFirst()) { //there is a result
                return;
            } else {
                PreparedStatement fetchStatement = IsaacDatabaseInitializer.getDatabaseConnection().prepareStatement(checkSQL1);
                try {
                    fetchStatement.setInt(1, Integer.parseInt(userID));
                } catch (NumberFormatException e) {
                    fetchStatement.setInt(1, -1);
                }
                fetchStatement.setString(2, userID);
                ResultSet fetchResults = fetchStatement.executeQuery();

                if (!fetchResults.isBeforeFirst()) { //there is no result
                    return;
                } else {
                    fetchResults.next();

                    final String insertSQL = "INSERT INTO user_table (user_id, gender, role, school_id, last_seen, questions_correct, location) VALUES " +
                            "(?, ?, ?, ?, ?, 0, ?)";
                    PreparedStatement insertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);

                    insertStatement.setString(1, userID);
                    insertStatement.setString(2, fetchResults.getString("gender"));
                    insertStatement.setString(3, fetchResults.getString("role"));
                    insertStatement.setInt(4, fetchResults.getInt("school_id"));
                    insertStatement.setDate(5, fetchResults.getDate("last_seen"));
                    insertStatement.setString(6, "");

                    insertStatement.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
