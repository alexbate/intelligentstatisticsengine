package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata.QuestionGetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by alex on 14/11/15.
 */
public class AnswerQuestionParser implements EventParser {

    @Override
    public boolean parseEvent(JSONObject json) {
        String questionID = null;
        String SQL = "", SQL2 = "", SQL3 = "", SQL4 = "", SQL5 = "";
        try {
            JSONObject details = json.getJSONObject("eventDetails");
            String userID = json.getString("userId");
            questionID = QuestionGetter.normaliseQuestionID(details.getString("questionId"));
            boolean correct = false;

            if (details.has("correct")) {
             correct = details.getBoolean("correct");
            }

            String incorrectAnswer = "";

            new QuestionGetter(questionID).getQuestionAndCreateRow();

            SQL2 = "INSERT INTO user_question (user_id, question_id, num_views, num_correct, num_incorrect) " +
                    "SELECT ?, ?, 0, ?, ? WHERE NOT EXISTS (SELECT 1 FROM user_question WHERE user_id=? AND question_id=?);";

            if (correct) {
                SQL = "UPDATE user_question SET num_correct = num_correct + 1 WHERE user_id = ? AND question_id = ?;";
                SQL3 = "UPDATE question SET num_correct_answers = num_correct_answers + 1 WHERE id = ?;";
                SQL4 = "UPDATE user_table SET questions_correct = questions_correct + 1 WHERE user_id = ?";
                SQL5 = "";
            } else {
                SQL = "UPDATE user_question SET num_incorrect = num_incorrect + 1 WHERE user_id = ? AND question_id = ?;";
                SQL3 = "UPDATE question SET num_incorrect_answers = num_incorrect_answers + 1 WHERE id = ?;";
                if (details.has("answer") && details.getJSONObject("answer").has("value")) {
                    incorrectAnswer = details.getJSONObject("answer").getString("value");
                } else {
                    incorrectAnswer = "";
                }
                SQL4 = "UPDATE question_answer SET frequency = frequency + 1 WHERE question_id = ? AND answer = ?;";
                SQL5 = "INSERT INTO question_answer (question_id, answer, frequency)" +
                        "SELECT ?, ?, 1 WHERE NOT EXISTS (SELECT 1 FROM question_answer " +
                        "WHERE question_id = ? AND answer = ?);";
            }


            PreparedStatement updateStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            PreparedStatement insertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL2);

            PreparedStatement questionStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL3);

            PreparedStatement questionAnswerUpdateStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL4);

            PreparedStatement questionAnswerInsertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL5);

            updateStatement.setString(1, userID);
            insertStatement.setString(1, userID);
            insertStatement.setString(5, userID);

            updateStatement.setString(2, questionID);
            insertStatement.setString(2, questionID);
            insertStatement.setString(6, questionID);
            questionStatement.setString(1, questionID);

            if (correct) {
                insertStatement.setInt(3, 1);
                insertStatement.setInt(4, 0);

                questionAnswerUpdateStatement.setString(1, userID);

            } else {
                insertStatement.setInt(4, 1);
                insertStatement.setInt(3, 0);

                questionAnswerUpdateStatement.setString(1, questionID);
                questionAnswerInsertStatement.setString(1, questionID);
                questionAnswerInsertStatement.setString(3, questionID);

                questionAnswerUpdateStatement.setString(2, incorrectAnswer);
                questionAnswerInsertStatement.setString(2, incorrectAnswer);
                questionAnswerInsertStatement.setString(4, incorrectAnswer);

            }

            if (updateStatement.executeUpdate() == 0) {
                insertStatement.execute();
            } else {
                insertStatement.close();
            }

            questionStatement.execute();

            if (questionAnswerUpdateStatement.executeUpdate() == 0 && !correct) {
                questionAnswerInsertStatement.executeUpdate();
            } else {
                questionAnswerInsertStatement.close();
            }
            return true;
        } catch (JSONException e) {
            System.err.println(json);
            e.printStackTrace(System.err);
            return false;
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            return false;
        }
    }
}
