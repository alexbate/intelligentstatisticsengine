package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONObject;

/**
 * Base interface for eventParser
 * Created by alex on 09/11/15.
 */
public interface EventParser {

    /**
     * Takes in a JSONObject representing an event and performs the necessary changes to the database
     * @param json The JSON of the log event
     * @return true if parse has been successful, false otherwise
     */
    boolean parseEvent(JSONObject json);

}
