package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata.QuestionGetter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 23/02/16.
 */
public class HintParser implements EventParser {
    @Override
    public boolean parseEvent(JSONObject json) {
        try {
            String userID = json.getString("userId");
            JSONObject details = json.getJSONObject("eventDetails");
            String questionId = QuestionGetter.normaliseQuestionID(details.getString("questionId"));
            int hintId = details.getInt("hintIndex");

            final String fetchSQL = "SELECT hints_used FROM user_question WHERE user_id = ? AND question_id = ?;";
            PreparedStatement fetchStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(fetchSQL);
            fetchStatement.setString(1, userID);
            fetchStatement.setString(2, questionId);
            ResultSet fetchResults = fetchStatement.executeQuery();

            if (fetchResults.next()) {
                int oldVal = fetchResults.getInt("hints_used");
                if (hintId+1 > oldVal) { //+1 as hints 0 indexed
                    final String updateSQL = "UPDATE user_question SET hints_used = ? WHERE user_id = ? AND question_id = ?";
                    PreparedStatement updateStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(updateSQL);
                    updateStatement.setInt(1, hintId + 1);
                    updateStatement.setString(2, userID);
                    updateStatement.setString(3, questionId);
                    updateStatement.execute();
                    return true;

                } else {
                    // Already used a later hint, we do nothing
                    return true;
                }
            } else {
                String insertSQL = "INSERT INTO user_question (user_id, question_id, num_views, num_correct, num_incorrect, hints_used) SELECT ?, ?, 0, 0, 0, ?";
                PreparedStatement insertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);
                insertStatement.setInt(3, hintId + 1);
                insertStatement.setString(1, userID);
                insertStatement.setString(2, questionId);
                insertStatement.execute();
                return true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
