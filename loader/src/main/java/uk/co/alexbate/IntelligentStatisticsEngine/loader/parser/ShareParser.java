package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.Configuration;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by alex on 14/11/15.
 */
public class ShareParser implements EventParser {

    @Override
    public boolean parseEvent(JSONObject json) {
        try {
            JSONObject details = json.getJSONObject("eventDetails");
            String shortURL = details.getString("shortURL");
            String longURL = "";
            try {
                longURL = getLongUrl(shortURL);
            } catch (IOException e) {
                // Continue, it will still be useful to know that the link is being clicked
                shortURL = "null";
            } catch (JSONException e) {
                // Continue, it will still be useful to know that the link is being clicked
                shortURL = "null";
            }

            String SQL = "UPDATE shares SET frequency = frequency + 1 WHERE link=?;";

            String SQL2 = "INSERT INTO shares (link, page, frequency) " +
                    "SELECT ?, ?, 1 WHERE NOT EXISTS (SELECT 1 FROM shares WHERE link=?);";
            PreparedStatement updateStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            PreparedStatement insertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL2);

            updateStatement.setString(1, shortURL);
            insertStatement.setString(1, shortURL);
            insertStatement.setString(3, shortURL);
            insertStatement.setString(2, longURL);

            if (updateStatement.executeUpdate() == 0) {
                insertStatement.execute();
            } else {
                insertStatement.close();
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace(System.err);
            return false;
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            return false;
        }
    }

    private String getLongUrl(String shortUrl) throws IOException, JSONException {
        OkHttpClient client = new OkHttpClient();
        final String URL = Configuration.GOOGLE_SHORT_URL_API_KEY + shortUrl;
        Request request = new Request.Builder().url(URL).build();
        Response response = client.newCall(request).execute();
        JSONObject json = new JSONObject(response.body().string());
        return json.getString("longUrl");
    }
}
