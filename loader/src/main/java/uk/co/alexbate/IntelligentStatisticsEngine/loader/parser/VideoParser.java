package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import com.squareup.okhttp.HttpUrl;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata.UserGetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by alex on 09/11/15.
 */
public class VideoParser implements EventParser {
    @Override
    public boolean parseEvent(JSONObject json) {
        try {
            JSONObject details = json.getJSONObject("eventDetails");
            String url = details.getString("videoUrl");
            HttpUrl parsedUrl = HttpUrl.parse(url);
            String id = parsedUrl.queryParameter("v");
            int timestamp = (int) Math.floor(details.getDouble("videoPosition"));

            String eventType = json.getString("eventType");

            new UserGetter(json.getString("userId")).createUserRowIfNecessary();

            String columnName;
            if (eventType.equals("VIDEO_PAUSE")) {
                columnName = "pause_frequency";
            } else if (eventType.equals("VIDEO_PLAY")) {
                columnName = "play_frequency";
            } else {
                return false;
            }

            String SQL = "UPDATE video_event SET " + columnName + " = " + columnName + " + 1 WHERE video_id= ? AND time_in_seconds = ?;";
            String SQL2 = "INSERT INTO video_event (video_id, time_in_seconds, pause_frequency, play_frequency) " +
                    "SELECT ?, ?, ?, ? WHERE NOT EXISTS (SELECT 1 FROM video_event WHERE video_id=?)";

            PreparedStatement updateStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            PreparedStatement insertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL2);

            updateStatement.setString(1, id);
            insertStatement.setString(1, id);
            insertStatement.setString(5, id);

            updateStatement.setInt(2, timestamp);
            insertStatement.setInt(2, timestamp);
            insertStatement.setInt(3, timestamp);

            if (eventType.equals("VIDEO_PAUSE")) {
                insertStatement.setInt(3, 1);
                insertStatement.setInt(4, 0);
            } else if (eventType.equals("VIDEO_PLAY")) {
                insertStatement.setInt(3, 0);
                insertStatement.setInt(4, 1);
            }

            if (updateStatement.executeUpdate() == 0) {
                insertStatement.execute();
            } else {
                insertStatement.close();
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace(System.err);
            return false;
        } catch (SQLException e) {
            e.printStackTrace(System.err);
            return false;
        }
    }
}
