package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata.QuestionGetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by alex on 14/11/15.
 */
public class ViewQuestionParser implements EventParser {
    @Override
    public boolean parseEvent(JSONObject json) {
        String SQL = "", SQL2 = "", SQL3 = "";
        try {
            JSONObject details = json.getJSONObject("eventDetails");
            String userID = json.getString("userId");
            String questionID = QuestionGetter.normaliseQuestionID(details.getString("questionId"));

            new QuestionGetter(questionID).getQuestionAndCreateRow();

            SQL = "UPDATE user_question SET num_views = num_views + 1 WHERE user_id = ? AND question_id = ?;";

            SQL2 = "INSERT INTO user_question (user_id, question_id, num_views, num_correct, num_incorrect) " +
             "SELECT ?, ?, 1, 0, 0 WHERE NOT EXISTS (SELECT 1 FROM user_question WHERE user_id= ? AND question_id=?);";

            SQL3 = "UPDATE question SET views = views + 1 WHERE id = ?;";
            PreparedStatement updateStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            PreparedStatement insertStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL2);
            PreparedStatement questionStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL3);

            updateStatement.setString(1, userID);
            insertStatement.setString(1, userID);
            insertStatement.setString(3, userID);

            updateStatement.setString(2, questionID);
            insertStatement.setString(2, questionID);
            insertStatement.setString(4, questionID);
            questionStatement.setString(1, questionID);

            if(updateStatement.executeUpdate() == 0) {
                insertStatement.execute();
            } else {
                insertStatement.close();
            }

            questionStatement.execute();
            return true;
        } catch (JSONException e) {
            e.printStackTrace(System.err);
            return false;
        } catch (SQLException e) {
            System.err.println(json);
            System.err.println(SQL + SQL2 + SQL3);
            e.printStackTrace(System.err);
            return false;
        }
    }
}
