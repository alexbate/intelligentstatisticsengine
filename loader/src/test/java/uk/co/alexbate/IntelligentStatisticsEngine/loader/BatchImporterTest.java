package uk.co.alexbate.IntelligentStatisticsEngine.loader;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 09/11/15.
 */
public class BatchImporterTest extends BaseTestClass {
    final String viewQuestionjson = "{\"timestamp\":\"date\",\"_id\":\"id\",\"eventDetails\":{\"questionId\":\"human_tower\",\"contentVersion\":\"content version\"},\"userId\":\"user1\",\"anonymousUser\":true,\"eventDetailsType\":\"com.google.common.collect.RegularImmutableMap\",\"eventType\":\"VIEW_QUESTION\",\"ipAddress\":\"0:0:0:0:0:0:0:1\"}\n";

    final String videoJson = "{\n" +
            "    \"userId\" : \"a user id\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"VIDEO_PAUSE\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"videoUrl\" : \"http://youtube.com/watch?v=test\",\n" +
            "        \"videoPosition\" : \"42.601\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\"\n" +
            "}";

    final String unknownJson= "{\n" +
            "    \"userId\" : \"a user id\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"not an event type\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"videoUrl\" : \"url1\",\n" +
            "        \"videoPosition\" : \"42.601\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\"\n" +
            "}";

    final String shareJson = "{\n" +
            "    \"_id\" : \"id\",\n" +
            "    \"userId\" : \"userid\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"USE_SHARE_LINK\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"shortURL\" : \"https://goo.gl/58K24M\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"192.168.0.1\",\n" +
            "    \"timestamp\" : \"a time\"\n" +
            "}";

    final String hintJson = "{\n" +
            "    \"_id\" : id,\n" +
            "    \"userId\" : \"userid\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"VIEW_HINT\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"questionId\" : \"broken_cannon|c70f26e6-60ed-4f41-94c1-2fe35c0118e5\",\n" +
            "        \"hintIndex\" : \"3\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\",\n" +
            "    \"timestamp\" : \"2014-08-30T16:45:06.975Z\"\n" +
            "}";

    @Test
    public void testParseAndProcessSingleEvent_shareParsed() throws Exception {
        assertTrue(BatchImporter.parseAndProcessSingleEvent(new JSONObject(shareJson)));
    }

    @Test
    public void testParseAndProcessSingleEvent_viewQuestionParsed() throws Exception {
        assertTrue(BatchImporter.parseAndProcessSingleEvent(new JSONObject(viewQuestionjson)));
    }

    @Test
    public void testParseAndProcessSingleEvent_videoParsed() throws Exception {
        assertTrue(BatchImporter.parseAndProcessSingleEvent(new JSONObject(videoJson)));
    }

    @Test
    public void testParseAndProcessSingleEvent_unknown() throws Exception {
        assertFalse(BatchImporter.parseAndProcessSingleEvent(new JSONObject(unknownJson)));
    }

    @Test
    public void testParseAndProcessSingleEvent_hint() throws Exception {
        assertTrue(BatchImporter.parseAndProcessSingleEvent(new JSONObject(hintJson)));
    }

    @Test
    public void testParseAndProcessMultipleEvents_noExceptionsAreThrown() throws Exception {
        JSONArray array = new JSONArray();
        array.put(unknownJson);
        array.put(videoJson);
    }

}