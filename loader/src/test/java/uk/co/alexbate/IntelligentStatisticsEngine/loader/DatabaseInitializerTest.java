package uk.co.alexbate.IntelligentStatisticsEngine.loader;

import org.junit.Before;
import org.junit.Test;

import javax.xml.crypto.Data;

import java.sql.Connection;
import java.sql.ResultSet;

import static org.junit.Assert.*;

/**
 * Created by alex on 08/11/15.
 */
public class DatabaseInitializerTest extends BaseTestClass {

    @Test
    public void testGetDatabaseConnection_returnsNotNull() throws Exception {
        assertNotNull(DatabaseInitializer.getDatabaseConnection());
    }

    @Test
    public void testGetDatabaseConnection_tablesCreated() throws Exception {
        Connection db = DatabaseInitializer.getDatabaseConnection();

        final String query = "SELECT table_schema, table_name FROM information_schema.tables ORDER BY table_schema, table_name";

        ResultSet results = db.createStatement().executeQuery(query);

        boolean foundTable = false;

        while(results.next()) {
            String schema = results.getString("table_schema");
            if (schema.equals("public")) {
                foundTable = true;
            }
        }

        assertTrue(foundTable);
    }
}