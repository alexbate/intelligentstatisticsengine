package uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata;

import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.BaseTestClass;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by alex on 15/11/15.
 */
public class QuestionGetterTest extends BaseTestClass {

    QuestionGetter getter;

    final String question = "human_tower";

    @Test
    public void testNormaliseInput_Bar() {
        final String in = "foo|bar";
        assertEquals("foo", QuestionGetter.normaliseQuestionID(in));
    }

    @Test
    public void testNormaliseInput_NoBar() {
        final String in = "foobar";
        assertEquals("foobar", QuestionGetter.normaliseQuestionID(in));
    }

    @Test
    public void testNormaliseInput_twoBars() {
        final String in = "foo|bar|1234";
        assertEquals("foo", QuestionGetter.normaliseQuestionID(in));
    }

    @Test
    public void testGetQuestionAndCreateRow_firstRow() throws Exception {
        getter = new QuestionGetter(question);
        getter.getQuestionAndCreateRow();

        ResultSet results = getQuestionRows();
        results.next();

        assertEquals(question, results.getString("id"));
        assertEquals(0, results.getInt("views"));
        assertEquals(0, results.getInt("num_correct_answers"));
        assertEquals(0, results.getInt("num_incorrect_answers"));
    }

    @Test
    public void testGetQuestionAndCreateRow_noSecondRow() throws Exception {
        getter = new QuestionGetter(question);
        getter.getQuestionAndCreateRow();
        getter.getQuestionAndCreateRow();

        ResultSet results = getQuestionRows();
        results.next();

        assertTrue(results.isLast());
    }


    private ResultSet getQuestionRows() throws SQLException {
        String SQL = "SELECT * FROM question";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }
}