package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.BaseTestClass;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * Created by alex on 09/11/15.
 */
public class AnswerQuestionParserTest extends BaseTestClass {

    AnswerQuestionParser parser;

    @Before
    public void setUp() {
        parser = new AnswerQuestionParser();
    }

    final String user1 = "user1";
    final String question1 = "human_tower";
    final String correct = "true";
    final String incorrect = "false";

    final String json1 = "{\n" +
            "    \"_id\" : \"id\" ,\n" +
            "    \"userId\" : \"";

    final String json2 = "\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"ANSWER_QUESTION\",\n" +
            "    \"eventDetailsType\" : \"com.google.common.collect.RegularImmutableMap\",\n" +
            "    \"eventDetails\" : {\n" +
                    "\"answer\" : " +
                    "{\n" +
                    "    \"type\" : \"quantity\",\n" +
                    "    \"children\" : [],\n" +
                    "    \"value\" : \"3\",\n" +
                    "    \"published\" : false\n" +
                    "},\n" +
            "        \"questionId\" : \"";
    final String json3 = "\",\n" +
            "        \"correct\" : \"";
    final String json4 = "\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\",\n" +
            "    \"timestamp\" : \"date\"\n" +
            "}";

    @Test
    public void testParseEvent_rowIsAddedNewCorrect() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + correct + json4));
        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 1);
        assertEquals(results.getInt("num_incorrect"), 0);

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(1, questions.getInt("num_correct_answers"));
    }

    @Test
    public void testParseEvent_correctIsIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + correct + json4));
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + correct + json4));

        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 2);
        assertEquals(results.getInt("num_incorrect"), 0);

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(2, questions.getInt("num_correct_answers"));
    }
    @Test
    public void testParseEvent_rowIsAddedNewIncorrect() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + incorrect + json4));

        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 1);

        ResultSet answers = getQuestionAnswerRows();
        answers.next();

        assertEquals(question1, answers.getString("question_id"));
        assertEquals("3", answers.getString("answer"));
        assertEquals(1, answers.getInt("frequency"));

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(1, questions.getInt("num_incorrect_answers"));
    }

    @Test
    public void testParseEvent_incorrectIsIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + incorrect + json4));
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + incorrect + json4));

        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 2);

        ResultSet answers = getQuestionAnswerRows();
        answers.next();

        assertEquals(question1, answers.getString("question_id"));
        assertEquals("3", answers.getString("answer"));
        assertEquals(2, answers.getInt("frequency"));

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(2, questions.getInt("num_incorrect_answers"));
    }

    @Test
    public void testParseEvent_correctAndIncorrectSameRow() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + correct + json4));
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3 + incorrect + json4));

        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 1);
        assertEquals(results.getInt("num_incorrect"), 1);

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(1, questions.getInt("num_correct_answers"));
        assertEquals(1, questions.getInt("num_incorrect_answers"));
    }

    private ResultSet getUserQuestionRows() throws SQLException {
        String SQL = "SELECT * FROM user_question";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }

    private ResultSet getQuestionAnswerRows() throws SQLException {
        String SQL = "SELECT * FROM question_answer";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }

    private ResultSet getQuestionRows() throws SQLException {
        String SQL = "SELECT * FROM question";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }
}
