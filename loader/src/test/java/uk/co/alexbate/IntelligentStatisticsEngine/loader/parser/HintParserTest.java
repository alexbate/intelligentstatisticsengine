package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.BaseTestClass;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.metadata.QuestionGetter;

import java.sql.ResultSet;
import java.sql.SQLException;

import static junit.framework.Assert.assertEquals;

/**
 * Created by alex on 25/02/16.
 */
public class HintParserTest extends BaseTestClass {
    HintParser parser;

    final String json1 = "{\n" +
            "    \"_id\" : id,\n" +
            "    \"userId\" : \"";

    final String userid = "userid";

    final String json2 = "\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"VIEW_HINT\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"questionId\" : \"";

    final String questionid = "broken_cannon|c70f26e6-60ed-4f41-94c1-2fe35c0118e5";

    final String json3 = "\",\n" +
            "        \"hintIndex\" : \"";

    final String hintIndex = "0";

    final String json4 = "\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\",\n" +
            "    \"timestamp\" : \"2014-08-30T16:45:06.975Z\"\n" +
            "}";

    @Before
    public void setup() {
        parser = new HintParser();
    }

    @Test
    public void testParseHint_rowIsAdded() throws Exception {
        parser.parseEvent(new JSONObject(json1 + userid + json2 + questionid + json3 + hintIndex + json4));
        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), userid);
        assertEquals(results.getString("question_id"), QuestionGetter.normaliseQuestionID(questionid));
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 0);
        assertEquals(results.getInt("hints_used"), 1);
    }

    @Test
    public void testParseHint_isIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json1 + userid + json2 + questionid + json3 + hintIndex + json4));
        parser.parseEvent(new JSONObject(json1 + userid + json2 + questionid + json3 + "2" + json4));
        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), userid);
        assertEquals(results.getString("question_id"), QuestionGetter.normaliseQuestionID(questionid));
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 0);
        assertEquals(results.getInt("hints_used"), 3);
    }

    @Test
    public void testParseHint_isNotDecremented() throws Exception {
        parser.parseEvent(new JSONObject(json1 + userid + json2 + questionid + json3 + "2" + json4));
        parser.parseEvent(new JSONObject(json1 + userid + json2 + questionid + json3 + "0" + json4));
        ResultSet results = getUserQuestionRows();
        results.next();

        assertEquals(results.getString("user_id"), userid);
        assertEquals(results.getString("question_id"), QuestionGetter.normaliseQuestionID(questionid));
        assertEquals(results.getInt("num_views"), 0);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 0);
        assertEquals(results.getInt("hints_used"), 3);
    }

    private ResultSet getUserQuestionRows() throws SQLException {
        String SQL = "SELECT * FROM user_question";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }

}
