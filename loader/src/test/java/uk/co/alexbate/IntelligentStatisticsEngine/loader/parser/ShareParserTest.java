package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.BaseTestClass;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Created by alex on 15/11/15.
 */
public class ShareParserTest extends BaseTestClass {

    ShareParser parser;

    final String longUrl = "https://isaacphysics.org/";
    final String shortUrl = "https://goo.gl/58K24M";

    final String json1 = "{\n" +
            "    \"_id\" : \"id\",\n" +
            "    \"userId\" : \"userid\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"USE_SHARE_LINK\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"shortURL\" : \"";

    final String json2 = "\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"192.168.0.1\",\n" +
            "    \"timestamp\" : \"a time\"\n" +
            "}";

    @Before
    public void setUp() {
        parser = new ShareParser();
    }

    @Test
    public void testParseEvent_newRowCreated() throws Exception {
        parser.parseEvent(new JSONObject(json1 + shortUrl + json2));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(shortUrl, results.getString("link"));
        assertEquals(longUrl, results.getString("page"));
        assertEquals(1, results.getInt("frequency"));
    }

    @Test
    public void testParseEvent_existingRowIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json1 + shortUrl + json2));
        parser.parseEvent(new JSONObject(json1 + shortUrl + json2));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(shortUrl, results.getString("link"));
        assertEquals(longUrl, results.getString("page"));
        assertEquals(2, results.getInt("frequency"));
    }

    private ResultSet getDatabaseRows() throws SQLException {
        String SQL = "SELECT * FROM shares";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }

}