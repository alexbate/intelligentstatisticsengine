package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.BaseTestClass;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * Created by alex on 09/11/15.
 */
public class VideoParserTest extends BaseTestClass {

    VideoParser parser;

    @Before
    public void setUp() {
        parser = new VideoParser();
    }

    final String url1 = "http://youtube.com/watch?v=id1";
    final String id = "id1";
    final int position1 = 42;

    final String json1 = "{\n" +
            "    \"userId\" : \"a user id\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"VIDEO_PAUSE\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"videoUrl\" : \""+ url1 +"\",\n" +
            "        \"videoPosition\" : \"42.601\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\"\n" +
            "}";

    final String json2 = "{\n" +
            "    \"userId\" : \"a user id\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"VIDEO_PLAY\",\n" +
            "    \"eventDetailsType\" : \"java.util.LinkedHashMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"videoUrl\" : \""+ url1 +"\",\n" +
            "        \"videoPosition\" : \"42.601\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\"\n" +
            "}";

    @Test
    public void testParseEvent_rowIsAddedNewPause() throws Exception {
        parser.parseEvent(new JSONObject(json1));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("video_id"), id);
        assertEquals(results.getInt("time_in_seconds"), position1);
        assertEquals(results.getInt("pause_frequency"), 1);
    }

    @Test
    public void testParseEvent_pauseIsIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json1));
        parser.parseEvent(new JSONObject(json1));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("video_id"), id);
        assertEquals(results.getInt("time_in_seconds"), position1);
        assertEquals(results.getInt("pause_frequency"), 2);
    }

    @Test
    public void testParseEvent_rowIsAddedNewPlay() throws Exception {
        parser.parseEvent(new JSONObject(json2));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("video_id"), id);
        assertEquals(results.getInt("time_in_seconds"), position1);
        assertEquals(results.getInt("play_frequency"), 1);
    }

    @Test
    public void testParseEvent_playIsIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json2));
        parser.parseEvent(new JSONObject(json2));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("video_id"), id);
        assertEquals(results.getInt("time_in_seconds"), position1);
        assertEquals(results.getInt("play_frequency"), 2);
    }

    @Test
    public void testParseEvent_playAndPauseSameRow() throws Exception {
        parser.parseEvent(new JSONObject(json1));
        parser.parseEvent(new JSONObject(json2));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("video_id"), id);
        assertEquals(results.getInt("time_in_seconds"), position1);
        assertEquals(results.getInt("play_frequency"), 1);
        assertEquals(results.getInt("pause_frequency"), 1);
    }

    private ResultSet getDatabaseRows() throws SQLException {
        String SQL = "SELECT * FROM video_event";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }
}