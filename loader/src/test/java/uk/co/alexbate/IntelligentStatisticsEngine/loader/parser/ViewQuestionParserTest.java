package uk.co.alexbate.IntelligentStatisticsEngine.loader.parser;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.BaseTestClass;
import uk.co.alexbate.IntelligentStatisticsEngine.loader.DatabaseInitializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * Created by alex on 09/11/15.
 */
public class ViewQuestionParserTest extends BaseTestClass {

    ViewQuestionParser parser;

    @Before
    public void setUp() {
        parser = new ViewQuestionParser();
    }

    final String user1 = "user1";
    final String question1 = "human_tower";

    final String json1 = "{\n" +
            "    \"_id\" : id ,\n" +
            "    \"userId\" : \"";

    final String json2 = "\",\n" +
            "    \"anonymousUser\" : true,\n" +
            "    \"eventType\" : \"VIEW_QUESTION\",\n" +
            "    \"eventDetailsType\" : \"com.google.common.collect.RegularImmutableMap\",\n" +
            "    \"eventDetails\" : {\n" +
            "        \"questionId\" : \"";
    final String json3 = "\",\n" +
            "        \"contentVersion\" : \"content version\"\n" +
            "    },\n" +
            "    \"ipAddress\" : \"0:0:0:0:0:0:0:1\",\n" +
            "    \"timestamp\" : \"date\"\n" +
            "}";

    @Test
    public void testParseEvent_rowIsAddedNewView() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 1);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 0);

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(1, questions.getInt("views"));
    }

    @Test
    public void testParseEvent_viewIsIncremented() throws Exception {
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3));
        parser.parseEvent(new JSONObject(json1 + user1 + json2 + question1 + json3));

        ResultSet results = getDatabaseRows();
        results.next();

        assertEquals(results.getString("user_id"), user1);
        assertEquals(results.getString("question_id"), question1);
        assertEquals(results.getInt("num_views"), 2);
        assertEquals(results.getInt("num_correct"), 0);
        assertEquals(results.getInt("num_incorrect"), 0);

        ResultSet questions = getQuestionRows();
        questions.next();

        assertEquals(question1, questions.getString("id"));
        assertEquals(2, questions.getInt("views"));
    }


    private ResultSet getDatabaseRows() throws SQLException {
        String SQL = "SELECT * FROM user_question";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }

    private ResultSet getQuestionRows() throws SQLException {
        String SQL = "SELECT * FROM question";
        return DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
    }
}