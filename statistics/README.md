## Statistics

### Installation guide
1. Fill out `.credentials` file with database credentials
2. Fill out `Configuration.java` with the required settings
3. `./gradlew statistics:run`

### Overview
This module reads the database maintatined by the `loader` module, and presents information from it via an API. For each piece of information it retrieves, it calculates an 'interesting score'. Background tasks maintain summary information for this data (mean, median etc) and find the 'most interesting' example for each piece of information retrievable by the API.

Machine learning can be used to improve these scores. One, simple method, chases some internal 'constants' used in calculating the score based on the feedback received. There is also a basic, slow implementation of the Q-learning algorithm (ideally a neural network would be used instead).

Finally, there is the ability for certain, key statistics to be pushed using the WebSocket protocol.
