package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

/**
 * Created by alex on 17/01/16.
 */
public class Configuration {

    //Parameters to alter the behaviour of the maxmini algorithm
    public static final int MAXIMINI_HIGH_FEEDBACK_THRESHOLD = 75;
    public static final int MAXIMINI_LOWER_FEEDBACK_THRESHOLD = 25;
    public static final double MAXIMINI_HIGHER_FEEDBACK_FACTOR = 1.1;
    public static final double MAXIMINI_LOWER_FEEDBACK_FACTOR = 0.9;

    public static String DATABASE_NAME = "stats_db_name";
    public static final String DB_URL = "localhost:path";
    public static final String KEYSTORE_PATH = "path/to/keystore";
    public static final String KEYSTORE_PASSWORD = "keystorepass";
    public static final String TRUSTSTORE_PATH = "path/to/truststore";
    public static final String TRUSTSTORE_PASSWORD = "truststorepass";
    //Switch to choose between QLearning and the classic metric calculation
    public static boolean useQLearning = false;

    //Threshold for a non-milestone statistics to be pushed to connnected websockets
    public static double PUSH_THRESHOLD = 90;

    //Interval for the last seen statistic
    public static String last_seen_interval = "107 days";

    public static boolean returnMean = true;

}
