package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;

/**
 * Created by alex on 08/11/15.
 */
public class DatabaseInitializer {
    private static Connection databaseConnection;
    private static Connection writableConnection;

    public static Connection getDatabaseConnection() {
        try {
            if (databaseConnection == null || databaseConnection.isClosed()) {
                databaseConnection = openDatabaseConnection();
                createTablesIfNecessary(getTableSchemas(), databaseConnection);
            }
        } catch (SQLException e) {
            System.err.println("Error checking if database is closed");
            System.exit(3);
        }

        return databaseConnection;
    }

    public static Connection getWritableConnection() {
        try {
            if (writableConnection == null || writableConnection.isClosed()) {
                writableConnection = openDatabaseConnection();
                createTablesIfNecessary(getTableSchemas(), writableConnection);
            }
        } catch (SQLException e) {
            System.err.println("Error checking if database is closed");
            System.exit(3);
        }

        try {
            writableConnection.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return writableConnection;
    }

    private static void createTablesIfNecessary(LinkedList<String> tableSchemas, Connection c) {
        for(String sql : tableSchemas) {
            try {
                Statement statement = c.createStatement();
                statement.execute(sql);
                statement.close();
            } catch (SQLException e) {
                System.err.println(sql);
                e.printStackTrace();
            }
        }
    }

    private static Connection openDatabaseConnection() {
        Connection c;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println("Database driver not found - fatal");
            System.exit(1);
        }

        // These credentials work for Travis CI, but can be overriden with a .credentials file
        String username = "postgres";
        String password = "";

        try {
            BufferedReader credentialReader = new BufferedReader(new FileReader(".credentials"));
            username = credentialReader.readLine();
            password = credentialReader.readLine();
        } catch (FileNotFoundException e) {
            // Use the defaults, all is well
        } catch (IOException e) {
            System.err.println("Error reading credentials file");
            System.exit(2);
        }

        try {
            c = DriverManager.getConnection("jdbc:postgresql:" + Configuration.DATABASE_NAME + "//" + Configuration.DB_URL, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            c = null;
        }

        return c;
    }

    private static LinkedList<String> getTableSchemas() {
        final LinkedList<String> sqlStatements = new LinkedList<String>();
        final String summaryTable = "CREATE TABLE IF NOT EXISTS summary\n" +
                "(\n" +
                "  id text NOT NULL,\n" +
                "  mean float,\n" +
                "  mode float,\n" +
                "  median float,\n" +
                "  stdev float\n" +
                ")";
        sqlStatements.add(summaryTable);

        final String pushTable = "CREATE TABLE IF NOT EXISTS push\n" +
                "(\n" +
                "  id serial PRIMARY KEY,\n" +
                "  key text NOT NULL,\n" +
                "  value numeric" +
                ")";
        sqlStatements.add(pushTable);

        final String statisticsTable = "CREATE TABLE IF NOT EXISTS statistics\n" +
                "(\n" +
                "  id text PRIMARY KEY,\n" +
                "  key text,\n" +
                "  value numeric,\n" +
                "  interested numeric,\n" +
                "  time timestamp default current_timestamp,\n" +
                "  mean numeric,\n" +
                "  median numeric,\n" +
                "  intrinsic numeric,\n" +
                "  milestone numeric,\n" +
                "  feedback numeric\n" +
                ")";
        sqlStatements.add(statisticsTable);

        final String paramsTable = "CREATE TABLE IF NOT EXISTS params\n" +
                "(\n" +
                "  seq SERIAL PRIMARY KEY,\n" +
                "  time timestamp default current_timestamp, \n" +
                "  milestone numeric, \n" +
                "  mean numeric, \n" +
                "  median numeric, \n" +
                "  intrinsic numeric \n" +
                ")";
        sqlStatements.add(paramsTable);

        final String schoolView = "CREATE OR REPLACE VIEW school_counts AS SELECT school_id, count(*) FROM user_table GROUP BY school_id;";
        sqlStatements.add(schoolView);

        final String user_question_view = "CREATE OR REPLACE VIEW user_question_views AS SELECT user_id, count(*), sum(hints_used) FROM user_question GROUP BY user_id";
        sqlStatements.add(user_question_view);

        final String question_hints_view = "CREATE OR REPLACE VIEW question_hints_view AS SELECT question_id, sum(hints_used) FROM user_question GROUP BY question_id;";
        sqlStatements.add(question_hints_view);

        final String user_question_details = "CREATE OR REPLACE VIEW user_question_details AS SELECT question_id, num_views, num_correct, num_incorrect, gender, role, hints_used FROM user_question, user_table" +
                " WHERE user_table.user_id = user_question.user_id";
        sqlStatements.add(user_question_details);

        final String roundNumberFunc = "CREATE OR REPLACE FUNCTION milestone(f numeric) RETURNS boolean AS $$\n" +
                "DECLARE\n" +
                "logInput numeric := 0;\n" +
                "BEGIN\n" +
                "logInput := log(10.0, f);\n" +
                "RETURN (logInput = round(logInput));\n" +
                "END;\n" +
                "$$ LANGUAGE plpgsql;";
        sqlStatements.add(roundNumberFunc);

        final String insertPushStat = "CREATE OR REPLACE FUNCTION insert_push(v numeric, k text) RETURNS boolean AS $$\n" +
                "BEGIN\n" +
                "    INSERT INTO push(key, value) VALUES (k, v);" +
                "    RETURN TRUE;\n" +
                "END;\n" +
                "$$ LANGUAGE plpgsql;";
        sqlStatements.add(insertPushStat);

        final String num_users_function = "CREATE OR REPLACE FUNCTION milestone_users() RETURNS TRIGGER AS $$\n" +
                "DECLARE\n" +
                "v numeric;\n" +
                "r boolean;\n" +
                "BEGIN\n" +

                "SELECT count(*) INTO v FROM user_table;\n" +
                "IF (milestone(v)) THEN \n" +
                "r := insert_push(v, 'total_users');\n" +
                "END IF;\n" +

                "SELECT COUNT(*) FROM user_table WHERE school_id is not NULL;\n" +
                "IF (milestone(v)) THEN \n" +
                "r := insert_push(v, 'users_with_school');\n" +
                "END IF;\n" +

                "RETURN NULL;\n" +
                "END;\n" +
                "$$ LANGUAGE plpgsql;";
        sqlStatements.add(num_users_function);

        final String numUsersTrigger = "CREATE TRIGGER num_users_trigger AFTER INSERT ON user_table EXECUTE PROCEDURE milestone_users();";
        sqlStatements.add(numUsersTrigger);

        final String question_function = "CREATE OR REPLACE FUNCTION milestone_question() RETURNS TRIGGER AS $$\n" +
                "DECLARE\n" +
                "v numeric;\n" +
                "r boolean;\n" +
                "BEGIN\n" +
                "SELECT count(views) INTO v FROM question;\n" +
                "IF (milestone(v)) THEN \n" +
                "r := insert_push(v, 'total_question_views');\n" +
                "END IF;\n" +

                "SELECT count(num_correct_answers) INTO v FROM question;\n" +
                "IF (milestone(v)) THEN \n" +
                "r := insert_push(v, 'total_questions_correct');\n" +
                "END IF;\n" +

                "SELECT count(num_incorrect_answers) INTO v FROM question;\n" +
                "IF (milestone(v)) THEN \n" +
                "r := insert_push(v, 'total_questions_incorrect');\n" +
                "END IF;\n" +

                "RETURN NULL;\n" +
                "END;\n" +
                "$$ LANGUAGE plpgsql;";
        sqlStatements.add(question_function);

        final String questionTrigger = "CREATE TRIGGER question_trigger AFTER INSERT OR UPDATE ON question EXECUTE PROCEDURE milestone_question();";
        sqlStatements.add(questionTrigger);



        return sqlStatements;

    }
}
