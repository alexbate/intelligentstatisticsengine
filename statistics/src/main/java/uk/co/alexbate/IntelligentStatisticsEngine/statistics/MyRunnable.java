package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by alex on 03/01/16.
 */
public abstract class MyRunnable implements Runnable {
    public void taskComplete() {
        try {
            Server.backgroundThreads.execute((Runnable) this.getClass().getConstructors()[0].newInstance(null));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println("Finished task " + this.getClass().getSimpleName());
    }
}
