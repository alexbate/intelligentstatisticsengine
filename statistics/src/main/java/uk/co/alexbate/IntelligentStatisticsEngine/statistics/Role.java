package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

/**
 * Created by alex on 14/12/15.
 */
public enum Role {
    STUDENT {
        @Override
        public String toString() {
            return "STUDENT";
        }
    },
    CONTENT_EDITOR {
        @Override
        public String toString() {
            return "CONTENT_EDITOR";
        }
    },
    TEACHER {
        @Override
        public String toString() {
            return "TEACHER";
        }
    },
    EVENT_MANAGER {
        @Override
        public String toString() {
            return "EVENT_MANAGER";
        }
    },
    ADMIN {
        @Override
        public String toString() {
            return "ADMIN";
        }
    };

    public static Role parseString(String s) {
       for (Role r : Role.values()) {
           if (r.toString().equals(s)) {
               return r;
           }
       }
        return null;
    }
    }
