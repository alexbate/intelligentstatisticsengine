package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator.QuestionAggregator;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator.SchoolAggregator;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator.UserAggregator;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator.VideoAggregator;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.*;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.learner.MaxiMinLearner;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.learner.QLearner;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;

import static spark.Spark.*;

/**
 * Created by alex on 25/11/15.
 */
public class Server {

    private static final BlockingQueue<Runnable> backgroundTasks = new ArrayBlockingQueue<Runnable>(1000);
    public static ThreadPoolExecutor backgroundThreads;

    public static void main(String[] args) {
        DatabaseInitializer.getDatabaseConnection();
        DatabaseInitializer.getWritableConnection();

        try {
            if (Configuration.useQLearning) {
                QLearner.setUpQPrimeTable(5);
            }
        } catch (SQLException e) {
            e.getNextException().printStackTrace();
            e.printStackTrace();
        }
        try {
            Statistic.updateParametersFromDB();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        port(4242);
        if (Files.exists(Paths.get(Configuration.KEYSTORE_PATH))) {
            if(Files.exists(Paths.get(Configuration.TRUSTSTORE_PATH))) {
                secure(Configuration.KEYSTORE_PATH, Configuration.KEYSTORE_PASSWORD, Configuration.TRUSTSTORE_PATH, Configuration.TRUSTSTORE_PASSWORD);
            }
        }

        webSocket("/push", WebSocket.class);

        startServer();
        createTimerTask();
        createPushTask();
    }



    public static void startServer() {
        get("/user", ((request, response) -> {
            response.type("application/json");
            return listToJson(new UserAggregator().getInterestingStats());
        }));

        get("/user/:userid", (request, response) -> {
            response.type("application/json");
            String userId = request.params(":userid");

            Getter getter = new UserGetter(userId);
            List<Statistic> stats = getter.getStatistics();

            return listToJson(stats);
        });

        get("/question", ((request, response) -> {
            response.type("application/json");
            return listToJson(new QuestionAggregator().getInterestingStats());
        }));

        get("/question/:questionid", ((request, response) -> {
            response.type("application/json");
            String questionId = request.params(":questionid");

            Getter getter = new QuestionGetter(questionId);
            List<Statistic> stats = getter.getStatistics();

            return listToJson(stats);
        }));

        get("/question/:questionid/:p1", ((request, response) -> {
            response.type("application/json");
            String questionId = request.params(":questionid");

            Role r = Role.parseString(request.params(":p1"));
            Getter g;
            if (r != null) {
                g = new QuestionGetter(questionId, r);
            } else {
                g = new QuestionGetter(questionId, request.params(":p1"));
            }

            List<Statistic> stats = g.getStatistics();

            return listToJson(stats);
        }));

        get("/question/:questionid/:gender/:role", ((request, response) -> {
            response.type("application/json");
            String questionId = request.params(":questionid");

            Role r = Role.parseString(request.params(":role"));
            Getter g;
            if (r != null) {
                g = new QuestionGetter(questionId, request.params(":gender"), r);
            } else {
                g = new QuestionGetter(questionId, request.params(":gender"));
            }

            List<Statistic> stats = g.getStatistics();

            return listToJson(stats);
        }));

        get("/summary", (request, response) -> {
            response.type("application/json");
            return listToJson(new SummaryGetter().getStatistics());
        });

        get("/summary/:gender", ((request, response) -> {
            response.type("application/json");
            Role r = Role.parseString(request.params(":gender"));
            Getter g;
            if (r != null) {
                g = new SummaryGetter(r);
            } else {
                g = new SummaryGetter(request.params(":gender"));
            }
            return listToJson(g.getStatistics());
        }));

        get("/school", ((request, response) -> {
            response.type("application/json");
            return listToJson(new SchoolAggregator().getInterestingStats());
        }));

        get("/school/:schoolid", (request, response) -> {
            response.type("application/json");
            String schoolId = request.params(":schoolid");

            return listToJson(new SchoolGetter(schoolId).getStatistics());
        });

        get("/school/none", (request, response) -> {
            response.type("application/json");
            return listToJson(new SchoolGetter().getStatistics());
        });

        get("/feedback/:statid/:rating", ((request, response) -> {
            Statistic.saveFeedback(request.params(":statid"), Double.parseDouble(request.params(":rating")));
            if (Configuration.useQLearning) {
                backgroundThreads.execute(new QLearner(request.params(":statid")));
            } else {
                backgroundThreads.execute(new MaxiMinLearner(request.params(":statid")));
            }
            return "";
        }));

        get("/video", ((request, response) -> {
            response.type("application/json");
            return listToJson(new VideoAggregator().getInterestingStats());
        }));

        get ("/video/:videoid", ((request, response) -> {
            response.type("application/json");
            return listToJson(new VideoGetter(request.params(":videoid")).getStatistics());
        }));

        get("/meta", ((request, response) -> {
            response.type("application/json");
            JSONObject json = new JSONObject();

            final String SQL = "select pg_size_pretty(pg_database_size('" + Configuration.DATABASE_NAME + "')), pg_database_size('" + Configuration.DATABASE_NAME + "');";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                JSONObject disk = new JSONObject();
                disk.put("raw", resultSet.getInt(2));
                disk.put("pretty", resultSet.getString(1));
                json.put("disk", disk);
            }

            json.put("queue", backgroundTasks.size());

            return json.toString();

        }));
    }

    private static String listToJson(List<Statistic> stats) throws JSONException, SQLException {
        JSONObject json = new JSONObject();

        Collections.sort(stats, (statistic, t1) -> {
            try {
                return (int) Math.round(statistic.getInterestedMetric() - t1.getInterestedMetric());
            } catch (SQLException e) {
                return (int) (statistic.getCoreValue() - t1.getCoreValue());
            }
        });

        for (Statistic s: stats) {
            JSONObject innerJson = statToJSON(s);

            if (!json.has(s.getKey())) {
                json.put(s.getKey(), new JSONArray());
            }

            json.put(s.getKey(), json.getJSONArray(s.getKey()).put(innerJson));
        }

        return json.toString();
    }

    public static JSONObject statToJSON(Statistic s) throws JSONException, SQLException {
        s.saveStatToDb();
        JSONObject innerJson = new JSONObject();
        innerJson.put("coreValue", s.getCoreValue());
        innerJson.put("explanation", s.toString());
        innerJson.put("interestingMetric", s.getInterestedMetric());
        innerJson.put("elementId", s.getElementId());
        innerJson.put("statId", s.getID());

        if (Configuration.returnMean) {
            final String SQL = "SELECT mean FROM summary WHERE id = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, s.getKey());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                innerJson.put("mean", resultSet.getDouble("mean"));
            }
        }

        return innerJson;
    }

    private static void createTimerTask() {

        final int CORE_THREADS = 0;
        final int MAX_THREADS = 1;

        backgroundThreads = new ThreadPoolExecutor(CORE_THREADS, MAX_THREADS, 10, TimeUnit.SECONDS, backgroundTasks, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread t = new Thread(runnable);
                t.setPriority(Thread.MIN_PRIORITY);
                return t;
            }
        });

        backgroundThreads.execute(new UserQuestionsCorrectSummary());
        backgroundThreads.execute(new QuestionSummary());
        backgroundThreads.execute(new SummarySummary());
        backgroundThreads.execute(new SchoolSummary());
        backgroundThreads.execute(new VideoSummary());

        backgroundThreads.execute(new QuestionAggregator());
        backgroundThreads.execute(new SchoolAggregator());
        backgroundThreads.execute(new UserAggregator());
        backgroundThreads.execute(new VideoAggregator());
    }

    private static void createPushTask() {
        TimerTask checkForPushTask = new TimerTask() {
            @Override
            public void run() {
                String SQL = "SELECT * FROM push;";
                try {
                    ResultSet results = DatabaseInitializer.getDatabaseConnection().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE).executeQuery(SQL);
                    while (results.next()) {
                        final String key = results.getString("key");
                        double val = results.getDouble("value");
                        WebSocket.sendStatistic(statToJSON(new Statistic(val) {
                            @Override
                            protected String getExplanation(double val) {
                                return "Statistic " + key + " pushed with value " + val;
                            }

                            @Override
                            protected double getIntrinsicMetric() {
                                return 1;
                            }

                            @Override
                            public String getKey() {
                                return key;
                            }

                            @Override
                            public String getElementId() {
                                return "push";
                            }
                        }));
                    }

                    results.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        final long milliseconds = 30 * 1000;

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(checkForPushTask, 0, milliseconds);
    }
}
