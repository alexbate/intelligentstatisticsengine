package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by alex on 18/12/15.
 */

@org.eclipse.jetty.websocket.api.annotations.WebSocket
public class WebSocket {
    private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();

    @OnWebSocketConnect
    public void connected(Session session) {
        sessions.add(session);
    }

    @OnWebSocketClose
    public void disconnected(Session session, int statusCode, String reason) {
        sessions.remove(session);
    }

    public static void sendStatistic(JSONObject json) throws IOException {
        for (Session s: sessions) {
            System.out.println(json.toString());
            s.getRemote().sendString(json.toString());
        }
    }

}
