package uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator;

import org.json.JSONException;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.*;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.Getter;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Task to get most interesting statistics
 * Created by alex on 17/12/15.
 */
public abstract class Aggregator extends MyRunnable {

    public static int NUM_STATISTICS_AGGREGATE = 5;

    protected abstract String getSQL();
    protected abstract String getColName();
    protected abstract Getter getNewGetter(String id);
    protected abstract HashMap<String, PriorityBlockingQueue<Statistic>> getMostInterestingStatistics();
    protected abstract void setMostInterestingStatistics(HashMap<String, PriorityBlockingQueue<Statistic>> map);

    public List<Statistic> getInterestingStats() throws SQLException, IOException, JSONException, InterruptedException {
        if (getMostInterestingStatistics() == null) {
            updateStatistics();
        }

        LinkedList<Statistic> output = new LinkedList<Statistic>();

        for (PriorityBlockingQueue<Statistic> q : getMostInterestingStatistics().values()) {
            output.addAll(q);
        }

        return output;
    }

    public void updateStatistics() throws SQLException, JSONException, IOException {
        HashMap<String, PriorityBlockingQueue<Statistic>> newInterestingStats = new HashMap<>();

        DatabaseInitializer.getDatabaseConnection().setAutoCommit(false);
        Statement statement = DatabaseInitializer.getDatabaseConnection().createStatement();
        statement.setFetchSize(10);
        ResultSet results = statement.executeQuery(getSQL());

        int i = 0;
        int p = 0;
        while (results.next()) {
            i++;
            if (i % 10 == 0) {
                System.out.println("Studying thing " + i);
            }
            Getter getter = getNewGetter(results.getString(getColName()));
            List<Statistic> stats = getter.getStatistics();
            for (Statistic s: stats) {
                if (s.getInterestedMetric() > Configuration.PUSH_THRESHOLD) {
                    WebSocket.sendStatistic(Server.statToJSON(s));
                    System.out.println("Pushing this " + ++i);
                }

                String key = s.getKey();
                if(!newInterestingStats.containsKey(key)) {
                    newInterestingStats.put(key, new PriorityBlockingQueue<Statistic>(NUM_STATISTICS_AGGREGATE, new Comparator<Statistic>() {
                        @Override
                        public int compare(Statistic statistic, Statistic t1) {
                            //Why not just do s-t1? Casting to int is fiddly because rounding
                            try {
                                double s = statistic.getInterestedMetric();
                                double t = t1.getInterestedMetric();
                                if (s < t) {
                                    return -1;
                                } else if (s > t){
                                    return 1;
                                } else {
                                    return 0;
                                }
                            } catch (SQLException e) {
                                return 0;
                            }
                        }
                    }));
                }

                addStatToHeap(newInterestingStats.get(key), s);
            }

        }
        setMostInterestingStatistics(newInterestingStats);
    }

    private void addStatToHeap(PriorityBlockingQueue<Statistic> q, Statistic s) throws SQLException {
        if (q.size() < NUM_STATISTICS_AGGREGATE) {
            q.add(s);
        } else {
            if (q.peek().getInterestedMetric() < s.getInterestedMetric()) {
                q.remove();
                q.add(s);
            }
        }
    }

    @Override
    public void run() {
        try {
            updateStatistics();
            this.taskComplete();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
