package uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.Getter;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.UserGetter;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.util.HashMap;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by alex on 17/12/15.
 */
public class UserAggregator extends Aggregator {
    private static HashMap<String, PriorityBlockingQueue<Statistic>> map;

    @Override
    protected HashMap<String, PriorityBlockingQueue<Statistic>> getMostInterestingStatistics() {
        return map;
    }

    @Override
    protected void setMostInterestingStatistics(HashMap<String, PriorityBlockingQueue<Statistic>> map) {
        this.map = map;
    }

    @Override
    protected String getSQL() {
        return "SELECT user_id FROM user_table;";
    }

    @Override
    protected String getColName() {
        return "user_id";
    }

    @Override
    protected Getter getNewGetter(String id) {
        return new UserGetter(id);
    }
}
