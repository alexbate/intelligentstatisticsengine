package uk.co.alexbate.IntelligentStatisticsEngine.statistics.aggregator;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.Getter;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.VideoGetter;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.util.HashMap;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by alex on 24/01/16.
 */
public class VideoAggregator extends Aggregator {
    private HashMap<String, PriorityBlockingQueue<Statistic>> stats = new HashMap<>();

    @Override
    protected String getSQL() {
        return "select distinct video_id from video_event";
    }

    @Override
    protected String getColName() {
        return "video_id";
    }

    @Override
    protected Getter getNewGetter(String id) {
        return new VideoGetter(id);
    }

    @Override
    protected HashMap<String, PriorityBlockingQueue<Statistic>> getMostInterestingStatistics() {
        return stats;
    }

    @Override
    protected void setMostInterestingStatistics(HashMap<String, PriorityBlockingQueue<Statistic>> map) {
        this.stats = map;
    }
}
