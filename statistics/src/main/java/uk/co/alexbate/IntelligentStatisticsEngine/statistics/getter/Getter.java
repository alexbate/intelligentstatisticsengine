package uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by alex on 25/11/15.
 */
public interface Getter {
    List<Statistic> getStatistics() throws SQLException;
}
