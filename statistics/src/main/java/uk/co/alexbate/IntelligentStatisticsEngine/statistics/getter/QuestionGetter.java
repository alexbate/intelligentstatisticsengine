package uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.Role;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 08/12/15.
 */
public class QuestionGetter implements Getter {
    final String questionId;
    final String gender;
    final Role role;

    public QuestionGetter(String questionId) {
        this.questionId = questionId;
        this.gender = null;
        this.role = null;
    }

    public QuestionGetter(String questionId, String gender) {
        this.questionId = questionId;
        this.gender = gender;
        this.role = null;
    }

    public QuestionGetter(String questionId, String gender, Role role) {
        this.questionId = questionId;
        this.gender = gender;
        this.role = role;
    }

    public QuestionGetter(String questionId, Role role) {
        this.questionId = questionId;
        this.gender = null;
        this.role = role;
    }

    @Override
    public List<Statistic> getStatistics() throws SQLException {
        ArrayList<Statistic> stats = new ArrayList<>();

        String SQL = "SELECT SUM(num_views) AS views, SUM(num_correct) AS correct, SUM(num_incorrect) AS incorrect, SUM(hints_used) AS hints FROM user_question_details WHERE question_id = ?";
        if (gender != null) {
            SQL += " AND gender = ?";
        }
        if (role != null) {
            SQL += " AND role = ?";
        }
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, questionId);
        if (gender != null) {
            statement.setString(2, gender);
            if (role != null) {
                statement.setString(3, role.toString());
            }
        } else if (role != null) {
            statement.setString(2, role.toString());
        }
        ResultSet results = statement.executeQuery();
        results.next();

        stats.add(new Statistic(results.getInt("views")) {
            @Override
            protected String getExplanation(double val) {
                return "This question has been viewed " + val + " times.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.2;
            }

            @Override
            public String getKey() {
                return "question_views";
            }

            @Override
            public String getElementId() {
                return questionId;
            }
        });

        stats.add(new Statistic(results.getInt("correct")) {
            @Override
            protected String getExplanation(double val) {
                return "This question has been answered correctly " + val + " times.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.5;
            }

            @Override
            public String getKey() {
                return "correct_answers";
            }

            @Override
            public String getElementId() {
                return questionId;
            }
        });

        stats.add(new Statistic(results.getInt("incorrect")) {
            @Override
            protected String getExplanation(double val) {
                return "This question has been answered incorrectly " + val + " times.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.5;
            }

            @Override
            public String getKey() {
                return "incorrect_answers";
            }

            @Override
            public String getElementId() {
                return questionId;
            }
        });

        stats.add(new Statistic(results.getInt("hints")) {
            @Override
            protected String getExplanation(double val) {
                return val + " hints have been used on this question";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.7;
            }

            @Override
            public String getKey() {
                return "question_hints";
            }

            @Override
            public String getElementId() {
                return questionId;
            }
        });

        return stats;
    }
}
