package uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import javax.xml.crypto.Data;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 11/12/15.
 */
public class SchoolGetter implements Getter {
    private final String schoolId;

    public SchoolGetter() {
        schoolId = null;
    }

    public SchoolGetter(String schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public List<Statistic> getStatistics() throws SQLException {
        LinkedList<Statistic> stats = new LinkedList<>();
        stats.add(getNumUsers());
        return stats;
    }

    private Statistic getNumUsers() throws SQLException {
        ResultSet results;
        if (schoolId == null || schoolId.equals("")) {
            String SQL = "SELECT COUNT(*) FROM user_table WHERE school_id IS NULL";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);

            results = statement.executeQuery();
        } else {
            String SQL = "SELECT COUNT(*) FROM user_table WHERE school_id = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setInt(1, Integer.parseInt(schoolId));
            results = statement.executeQuery();
        }

        results.next();

        return new Statistic(results.getInt("count")) {
            @Override
            protected String getExplanation(double val) {
                return "There are " + val + " users in this school";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.3;
            }

            @Override
            public String getKey() {
                return "school_users";
            }

            @Override
            public String getElementId() {
                return schoolId;
            }
        };
    }
}
