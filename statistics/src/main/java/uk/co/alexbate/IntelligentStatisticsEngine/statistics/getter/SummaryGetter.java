package uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.Configuration;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.Role;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 09/12/15.
 */
public class SummaryGetter implements Getter {

    final private String gender;
    final private Role role;

    public SummaryGetter() {
        this.gender = null;
        role = null;
    }

    public SummaryGetter(String gender) {
        this.gender = gender;
        role = null;
    }

    public SummaryGetter(Role r) {
        this.gender = null;
        this.role = r;
    }

    @Override
    public List<Statistic> getStatistics() throws SQLException {
        LinkedList<Statistic> stats = new LinkedList<>();
        stats.add(getTotalViews());
        stats.add(getTotalCorrect());
        stats.add(getTotalIncorrect());
        stats.add(getNumUsers());
        stats.add(getUsersWithSchool());
        stats.add(getUserslastInterval(Configuration.last_seen_interval));
        return stats;
    }

    private Statistic getTotalViews() throws SQLException {
        String SQL = "SELECT SUM(j.num_views) FROM (SELECT num_views, gender, role FROM user_question, user_table WHERE user_question.user_id = user_table.user_id) AS j";
        ResultSet results;
        if (gender != null) {
            SQL += " WHERE gender = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, gender);
            results = statement.executeQuery();
        } else if (role != null) {
            SQL += " WHERE role = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, role.toString());
            results = statement.executeQuery();
        } else {
            results = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        }
        results.next();
        int views = results.getInt("sum");
        return new Statistic(views) {
            @Override
            protected String getExplanation(double val) {
                return "There have been a total of " + val + " question views.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.2;
            }

            @Override
            public String getKey() {
                return "total_question_views";
            }

            @Override
            public String getElementId() {
                String id = "";
                if (gender != null) {
                    id += gender + " ";
                }
                if (role != null) {
                    id = role.toString();
                }

                if (id.equals("")) {
                    return "global";
                } else {
                    return id;
                }
            }
        };
    }

    private Statistic getTotalCorrect() throws SQLException {
        String SQL = "SELECT SUM(j.num_correct) FROM (SELECT num_correct, gender, role FROM user_question, user_table WHERE user_question.user_id = user_table.user_id) AS j";
        ResultSet results;
        if (gender != null) {
            SQL += " WHERE j.gender = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, gender);
            results = statement.executeQuery();
        } else if (role != null) {
            SQL += " WHERE j.role = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, role.toString());
            results = statement.executeQuery();
        } else {
            results = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        }
        results.next();
        int views = results.getInt("sum");
        return new Statistic(views) {
            @Override
            protected String getExplanation(double val) {
                return "There have been a total of " + val + " correct answers.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.4;
            }

            @Override
            public String getKey() {
                return "total_questions_correct";
            }

            @Override
            public String getElementId() {
                String id = "";
                if (gender != null) {
                    id += gender + " ";
                }
                if (role != null) {
                    id = role.toString();
                }

                if (id.equals("")) {
                    return "global";
                } else {
                    return id;
                }
            }
        };
    }

    private Statistic getTotalIncorrect() throws SQLException {
        String SQL = "SELECT SUM(j.num_incorrect) FROM (SELECT num_incorrect, gender, role FROM user_question, user_table WHERE user_question.user_id = user_table.user_id) AS j";
        ResultSet results;
        if (gender != null) {
            SQL += " WHERE j.gender = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, gender);
            results = statement.executeQuery();
        } else if (role != null) {
            SQL += " WHERE j.role = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, role.toString());
            results = statement.executeQuery();
        } else {
            results = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        }
        results.next();
        int views = results.getInt("sum");
        return new Statistic(views) {
            @Override
            protected String getExplanation(double val) {
                return "There have been a total of " + val + " incorrect answers.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.4;
            }

            @Override
            public String getKey() {
                return "total_questions_incorrect";
            }

            @Override
            public String getElementId() {
                String id = "";
                if (gender != null) {
                    id += gender + " ";
                }
                if (role != null) {
                    id = role.toString();
                }

                if (id.equals("")) {
                    return "global";
                } else {
                    return id;
                }
            }
        };
    }

    private Statistic getNumUsers() throws SQLException {
        String SQL = "SELECT COUNT(*) FROM user_table";
        ResultSet results;
        if (gender != null) {
           SQL += " WHERE gender = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, gender);
            results = statement.executeQuery();
        } else if (role != null) {
            SQL += " WHERE role = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, role.toString());
            results = statement.executeQuery();
        } else {
            results = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        }
        results.next();
        int users = results.getInt("count");
        return new Statistic(users) {
            @Override
            protected String getExplanation(double val) {
                return "There are " + val + " total users on the system.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.3;
            }

            @Override
            public String getKey() {
                return "total_users";
            }

            @Override
            public String getElementId() {
                String id = "";
                if (gender != null) {
                    id += gender + " ";
                }
                if (role != null) {
                    id = role.toString();
                }

                if (id.equals("")) {
                    return "global";
                } else {
                    return id;
                }
            }
        };
    }

    private Statistic getUsersWithSchool() throws SQLException {
        String SQL = "SELECT COUNT(*) FROM user_table WHERE school_id <> 0";
        ResultSet results;
        if (gender != null) {
            SQL += " AND gender = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, gender);
            results = statement.executeQuery();
        } else if (role != null) {
            SQL += " AND role = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, role.toString());
            results = statement.executeQuery();
        } else {
            results = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        }
        results.next();
        int users = results.getInt("count");
        return new Statistic(users) {
            @Override
            protected String getExplanation(double val) {
                return "There are " + val + " users who have added a school to their profile.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.5;
            }

            @Override
            public String getKey() {
                return "users_with_school";
            }

            @Override
            public String getElementId() {
                String id = "";
                if (gender != null) {
                    id += gender + " ";
                }
                if (role != null) {
                    id = role.toString();
                }

                if (id.equals("")) {
                    return "global";
                } else {
                    return id;
                }
            }
        };
    }

    private Statistic getUserslastInterval(String interval) throws SQLException {
        String SQL = "SELECT COUNT(*) FROM user_table WHERE last_seen > (CURRENT_DATE - INTERVAL '" + interval + "')";
        ResultSet results;
        if (gender != null) {
            SQL += " AND gender = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, gender);
            results = statement.executeQuery();
        } else if (role != null) {
            SQL += " AND role = ?";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, role.toString());
            results = statement.executeQuery();
        } else {
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            results = statement.executeQuery();
        }
        results.next();
        int users = results.getInt("count");
        return new Statistic(users) {
            @Override
            protected String getExplanation(double val) {
                return "There are " + val + " users who have been active in the last " + interval + ".";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.5;
            }

            @Override
            public String getKey() {
                return "last_seen";
            }

            @Override
            public String getElementId() {
                String id = "";
                if (gender != null) {
                    id += gender + " ";
                }
                if (role != null) {
                    id = role.toString();
                }

                if (id.equals("")) {
                    return "global";
                } else {
                    return id;
                }
            }
        };
    }
}
