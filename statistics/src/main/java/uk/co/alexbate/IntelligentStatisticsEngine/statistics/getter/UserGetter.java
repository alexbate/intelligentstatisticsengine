package uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 25/11/15.
 */
public class UserGetter implements Getter {
    final String userid;

    public UserGetter(String userid) {
        this.userid = userid;
    }

    @Override
    public List<Statistic> getStatistics() throws SQLException {
        ArrayList<Statistic> stats = new ArrayList<>();
        stats.add(getNumCorrect());
        stats.add(getViews());
        stats.add(getHints());
        Statistic leaderboard = getLeaderboardPosition();
        if (leaderboard != null) {
            stats.add(leaderboard);
        }
        return stats;
    }

    private Statistic getNumCorrect() throws SQLException {
        final String SQL = "SELECT questions_correct FROM user_table WHERE user_id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, userid);
        ResultSet resultSet = statement.executeQuery();

        int numCorrect;

        if (resultSet.next()) {
            numCorrect = resultSet.getInt("questions_correct");
        } else {
            numCorrect = 0;
        }

        return new Statistic(numCorrect) {
            @Override
            protected String getExplanation(double val) {
                return "This user has answered " + val + " questions correctly";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.6;
            }

            @Override
            public String getKey() {
                return "questions_correct";
            }

            @Override
            public String getElementId() {
                return userid;
            }
        };
    }

    private Statistic getViews() throws SQLException {
        final String SQL = "SELECT count(*) FROM user_question WHERE user_id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, userid);
        ResultSet resultSet = statement.executeQuery();
        int views;
        if (resultSet.next()) {
            views = resultSet.getInt("count");
        } else {
            views = 0;
        }
        return new Statistic(views) {
            @Override
            protected String getExplanation(double val) {
                return "This user has viewed " + val + " questions.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.5;
            }

            @Override
            public String getKey() {
                return "user_questions_viewed";
            }

            @Override
            public String getElementId() {
                return userid;
            }
        };
    }

    private Statistic getHints() throws SQLException {
        final String SQL = "SELECT sum(hints_used) FROM user_question WHERE user_id =?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, userid);
        ResultSet resultSet = statement.executeQuery();
        int views;
        if (resultSet.next()) {
            views = resultSet.getInt("sum");
        } else {
            views = 0;
        }
        return new Statistic(views) {
            @Override
            protected String getExplanation(double val) {
                return "This user has viewed " + val + " hints.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.7;
            }

            @Override
            public String getKey() {
                return "user_hints";
            }

            @Override
            public String getElementId() {
                return userid;
            }
        };
    }

    private Statistic getLeaderboardPosition() throws SQLException {
        final String SQL = "SELECT q.position FROM " +
                "(SELECT user_id, questions_correct, ROW_NUMBER() OVER(ORDER BY questions_correct DESC) AS position FROM user_table) AS q WHERE user_id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, userid);
        ResultSet results = statement.executeQuery();
        int position;
        if (results.next()) {
            position = results.getInt("position");
        } else {
            position = -1;
        }
        if (position == -1) {
            return null;
        }
        return new Statistic(position) {
            @Override
            protected String getExplanation(double val) {
                return "This user is at position " + val + " on the leaderboard.";
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0.8;
            }

            @Override
            public String getKey() {
                return "leaderboard";
            }

            @Override
            public String getElementId() {
                return userid;
            }
        };
    }

}
