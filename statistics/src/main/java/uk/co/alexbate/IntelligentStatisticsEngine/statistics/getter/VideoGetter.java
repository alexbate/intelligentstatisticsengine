package uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 24/01/16.
 */
public class VideoGetter implements Getter {
    private String videoID;

    public VideoGetter(String id) {
        this.videoID = id;
    }

    @Override
    public List<Statistic> getStatistics() throws SQLException {
        List<Statistic> stats = new LinkedList<>();
        stats.add(getNumPlays());
        stats.add(getMostPauses());
        return stats;
    }



    private Statistic getNumPlays() throws SQLException {
        final String SQL = "SELECT play_frequency FROM video_event WHERE time_in_seconds = 0 AND video_id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, videoID);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            return new Statistic(resultSet.getInt("play_frequency")) {
                @Override
                protected String getExplanation(double val) {
                    return "This video has been played from the start " + val + " times.";
                }

                @Override
                protected double getIntrinsicMetric() {
                    return 0.6;
                }

                @Override
                public String getKey() {
                    return "video_views";
                }

                @Override
                public String getElementId() {
                    return videoID;
                }
            };
        } else {
            return null;
        }
    }

    private Statistic getMostPauses() throws SQLException {
        final String SQL = "SELECT time_in_seconds, pause_frequency FROM video_event WHERE video_id = ? AND time_in_seconds <> 0 ORDER BY pause_frequency DESC LIMIT 1;";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, videoID);
        ResultSet results = statement.executeQuery();

        if (results.next()) {
            return new Statistic(results.getInt("pause_frequency"), results.getInt("time_in_seconds")) {
                @Override
                protected String getExplanation(double val) {
                    return "The most pauses are at " + secondaryValue + " seconds, with " + val + " events.";
                }

                @Override
                protected double getIntrinsicMetric() {
                    return 0.9;
                }

                @Override
                public String getKey() {
                    return "video_most_pauses";
                }

                @Override
                public String getElementId() {
                    return videoID;
                }
            };
        } else {
            return null;
        }

    }
}
