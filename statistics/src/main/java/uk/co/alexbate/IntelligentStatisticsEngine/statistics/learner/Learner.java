package uk.co.alexbate.IntelligentStatisticsEngine.statistics.learner;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.MyRunnable;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.security.InvalidParameterException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 13/01/16.
 */
public abstract class Learner extends MyRunnable {
    String statId, key;
    double feedback, mean, median, intrinsic, milestone, interesting;

    enum MetricType {
        MEAN, MEDIAN, MILESTONE, INTRINSIC
    }

    public abstract void learnFromFeedback() throws SQLException;

    @Override
    public void run() {
        try {
            learnFromFeedback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Learner(String id) {
        this.statId = id;
        final String statSQL = "SELECT * FROM statistics WHERE id = ?;";
        try {
            PreparedStatement statisticStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(statSQL);
            statisticStatement.setString(1, id);
            ResultSet results = statisticStatement.executeQuery();
            if (results.next()) {
                key = results.getString("key");
                feedback = results.getDouble("feedback");
                mean = results.getDouble("mean");
                median = results.getDouble("median");
                milestone = results.getDouble("milestone");
                intrinsic = results.getDouble("intrinsic");
                interesting = results.getDouble("interested");
            } else {
                throw new InvalidParameterException("No statistic found with id " + id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void changeMetric(MetricType type, double delta) throws SQLException {
        switch (type) {
            case MEAN:
                Statistic.updateMeanAlpha(Statistic.meanAlpha + delta);
                break;
            case MEDIAN:
                Statistic.updateMedianAlpha(Statistic.medianAlpha + delta);
                break;
            case INTRINSIC:
                Statistic.updateIntrinsicAlpha(Statistic.intrinsicAlpha + delta);
                break;
            case MILESTONE:
                Statistic.updateMilestoneAlpha(Statistic.nextMilestoneAlpha + delta);
                break;
        }
    }

    void multiplyMetric(MetricType type, double multiplier) throws SQLException {
        switch (type) {
            case MEAN:
                Statistic.updateMeanAlpha(Statistic.meanAlpha * multiplier);
                break;
            case MEDIAN:
                Statistic.updateMedianAlpha(Statistic.medianAlpha * multiplier);
                break;
            case INTRINSIC:
                Statistic.updateIntrinsicAlpha(Statistic.intrinsicAlpha * multiplier);
                break;
            case MILESTONE:
                Statistic.updateMilestoneAlpha(Statistic.nextMilestoneAlpha * multiplier);
                break;
        }
    }
}
