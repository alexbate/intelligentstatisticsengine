package uk.co.alexbate.IntelligentStatisticsEngine.statistics.learner;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.Configuration;

import java.sql.SQLException;

/**
 * Created by alex on 13/01/16.
 */
public class MaxiMinLearner extends Learner {

    public MaxiMinLearner(String id) {
        super(id);
    }

    private MetricType getMaxMetric() {
        if (mean > median) {
            if (milestone > mean) {
                return (intrinsic > milestone) ? MetricType.INTRINSIC : MetricType.MILESTONE;
            } else {
                return (intrinsic > mean) ? MetricType.INTRINSIC : MetricType.MEAN;
            }
        } else {
            if (milestone > median) {
                return (intrinsic > milestone) ? MetricType.INTRINSIC : MetricType.MILESTONE;
            } else {
                return (intrinsic > median) ? MetricType.INTRINSIC : MetricType.MEDIAN;
            }
        }
    }

    private MetricType getMinMetric() {
        if (mean <= median) {
            if (milestone <= mean) {
                return (intrinsic <= milestone) ? MetricType.INTRINSIC : MetricType.MILESTONE;
            } else {
                return (intrinsic <= mean) ? MetricType.INTRINSIC : MetricType.MEAN;
            }
        } else {
            if (milestone <= median) {
                return (intrinsic <= milestone) ? MetricType.INTRINSIC : MetricType.MILESTONE;
            } else {
                return (intrinsic <= median) ? MetricType.INTRINSIC : MetricType.MEDIAN;
            }
        }
    }

    @Override
    public void learnFromFeedback() throws SQLException {
        MetricType toIncrease = null;
        MetricType toDecrease = null;
        if (feedback > Configuration.MAXIMINI_HIGH_FEEDBACK_THRESHOLD) {
            toIncrease = getMaxMetric();
            toDecrease = getMinMetric();
        } else if (feedback < Configuration.MAXIMINI_LOWER_FEEDBACK_THRESHOLD) {
            toIncrease = getMinMetric();
            toDecrease = getMaxMetric();
        }

        multiplyMetric(toIncrease, Configuration.MAXIMINI_HIGHER_FEEDBACK_FACTOR);
        multiplyMetric(toDecrease, Configuration.MAXIMINI_LOWER_FEEDBACK_FACTOR);
    }
}
