package uk.co.alexbate.IntelligentStatisticsEngine.statistics.learner;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.statKeys;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 17/01/16.
 */
public class QLearner extends Learner  {

    private static double interval;

    public QLearner(String id) {
        super(id);
    }

    @Override
    public void learnFromFeedback() throws SQLException {
        final String getQPrimeRecord = "SELECT qprime, num_updates FROM qprime WHERE key = ? and mean = ? and median = ? and milestone = ? and output = ?";
        PreparedStatement qstatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(getQPrimeRecord);
        qstatement.setString(1, key);
        qstatement.setDouble(2, roundUpToInterval(mean));
        qstatement.setDouble(3, roundUpToInterval(median));
        qstatement.setDouble(4, roundUpToInterval(milestone));
        qstatement.setDouble(5, roundUpToInterval(interesting));
        ResultSet resultSet = qstatement.executeQuery();

        if (resultSet.next()) {
            double theta = 1 / (1 + resultSet.getDouble("num_updates"));

            double newQ = (1 - theta) * resultSet.getDouble("qprime") + theta * (feedback + maxForNextStat());

            final String updateSQL = "UPDATE qprime SET qprime = ?, num_updates = num_updates + 1 WHERE key = ? AND mean = ? AND median = ? AND milestone = ? AND output = ?;";
            PreparedStatement updateStatement = DatabaseInitializer.getWritableConnection().prepareStatement(updateSQL);
            updateStatement.setDouble(1, newQ);
            updateStatement.setString(2, key);
            updateStatement.setDouble(3, roundUpToInterval(mean));
            updateStatement.setDouble(4, roundUpToInterval(median));
            updateStatement.setDouble(5, roundUpToInterval(milestone));
            updateStatement.setDouble(6, roundUpToInterval(interesting));

            updateStatement.execute();
        }
    }

    private double maxForNextStat() throws SQLException {
        final String lastStatSQL = "SELECT * FROM statistics WHERE feedback IS NULL ORDER BY time desc LIMIT 1";
        ResultSet resultSet = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(lastStatSQL);
        if (resultSet.next()) {
            double lastMean = resultSet.getDouble("mean");
            double lastMedian = resultSet.getDouble("median");
            double lastMilestone = resultSet.getDouble("milestone");
            String lastKey = resultSet.getString("key");

            final String getQPrimeRecord = "SELECT max(qprime) FROM qprime WHERE key = ? and mean = ? and median = ? and milestone = ?";
            PreparedStatement qstatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(getQPrimeRecord);
            qstatement.setString(1, lastKey);
            qstatement.setDouble(2, roundUpToInterval(lastMean));
            qstatement.setDouble(3, roundUpToInterval(lastMedian));
            qstatement.setDouble(4, roundUpToInterval(lastMilestone));
            ResultSet resultSet1 = qstatement.executeQuery();

            if (resultSet1.next()) {
                return resultSet1.getDouble("max");
            }
        }
        return 0;
    }

    public static double roundUpToInterval(double input) {
        if(input == 0) {
            return interval;
        }
        if (input % interval != 0) {
            input += (interval - input % interval);
        }
        return input;
    }

    public static void setUpQPrimeTable(double interval) throws SQLException {
        QLearner.interval = interval;
        final String checkSQL = "SELECT COUNT(*) FROM qprime;";
        try {
            ResultSet r = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(checkSQL);
            if (r.getInt("count") != 0 ) {
                return;
            }
        } catch (SQLException e) {
            //Do nothing, create the table
        }

        final String qprimeTable = "CREATE TABLE IF NOT EXISTS qprime (" +
                " key text,\n" +
                " mean numeric,\n" +
                " median numeric,\n" +
                " milestone numeric,\n" +
                " output numeric, \n" +
                " qprime numeric,\n" +
                " num_updates numeric,\n" +
                " PRIMARY KEY(key, mean, median, milestone, output)\n" +
                ");";
        DatabaseInitializer.getWritableConnection().createStatement().execute(qprimeTable);

        final String SQL = "INSERT INTO qprime (key, mean, median, milestone, output, qprime, num_updates) VALUES (?, ?, ?, ?, ?, ?, '0');";
        PreparedStatement statement = DatabaseInitializer.getWritableConnection().prepareStatement(SQL);

        for (statKeys key : statKeys.values()) {
            statement.setString(1, key.name());
            for (double mean = interval; mean <= 100; mean += interval) {
                statement.setDouble(2, mean);
                for (double median = interval; median <= 100; median += interval) {
                    statement.setDouble(3, median);
                    for (double milestone = interval; milestone <= 100; milestone += interval) {
                        statement.setDouble(4, milestone);
                        for (double output = interval; output <= 100; output += interval) {
                            statement.setDouble(5, output);
                            double q1 = 100 - Math.abs(((mean + median + milestone) / 3) - output);
                            statement.setDouble(6, q1);
                            statement.addBatch();
                        }
                    }
                    statement.executeBatch();
                }
            }
        }

        statement.executeBatch();
    }
}
