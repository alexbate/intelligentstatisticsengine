package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

/**
 * Created by alex on 25/04/16.
 */
public enum statKeys {
    video_views, video_most_pauses, incorrect_answers, correct_answers, question_views, school_users, last_seen, users_with_school, total_users, total_questions_incorrect, total_questions_correct, total_question_views, questions_correct, user_questions_viewed, leaderboard, question_hints, user_hints
}
