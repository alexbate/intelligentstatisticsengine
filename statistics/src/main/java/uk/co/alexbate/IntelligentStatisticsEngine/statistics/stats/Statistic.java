package uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.Configuration;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.learner.QLearner;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by alex on 25/11/15.
 */
public abstract class Statistic {
    protected double coreValue;
    protected double secondaryValue;
    private String id;

    public static double nextMilestoneAlpha = 0.3;
    public static double meanAlpha = 0.3;
    public static double medianAlpha = 0.3;
    public static double intrinsicAlpha = 0.1;

    private double interested = -1;

    public static void saveFeedback(String id, double feedbackValue) throws SQLException {
        String SQL = "UPDATE statistics SET feedback = ? WHERE id = ?;";
        PreparedStatement statement = DatabaseInitializer.getWritableConnection().prepareStatement(SQL);
        statement.setDouble(1, feedbackValue);
        statement.setString(2, id);
        statement.execute();
    }

    public Statistic(double coreValue) {
        this.coreValue = coreValue;
        this.id = UUID.randomUUID().toString();
    }

    public Statistic(double coreValue, double secondary) {
        this.coreValue = coreValue;
        this.id = UUID.randomUUID().toString();
        this.secondaryValue = secondary;
    }

    public String getID() {
        return id;
    }

    public void saveStatToDb() {
        String SQL = "INSERT INTO statistics (id, key, mean, median, milestone, intrinsic, value) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement statement = DatabaseInitializer.getWritableConnection().prepareStatement(SQL);
            statement.setString(1, this.id);
            statement.setString(2, getKey());
            statement.setDouble(3, getMeanMetric());
            statement.setDouble(4, getMedianMetric());
            statement.setDouble(5, getMilestoneMetric());
            statement.setDouble(6, getIntrinsicMetric());
            statement.setDouble(7, coreValue);
            statement.execute();
            if(!DatabaseInitializer.getWritableConnection().getAutoCommit()) {
                DatabaseInitializer.getWritableConnection().commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double getInterestedMetric() throws SQLException {
        if (interested == -1) {
            if (!Configuration.useQLearning) {
                double metric = (nextMilestoneAlpha * getMilestoneMetric()) + (meanAlpha * getMeanMetric()) + (intrinsicAlpha * getIntrinsicMetric()) + (medianAlpha * getMedianMetric());
                if (metric < 0 || metric == Double.NEGATIVE_INFINITY) {
                    System.err.println("Metric too low " + metric);
                    metric = 0;
                } else if (metric > 100 || metric == Double.POSITIVE_INFINITY) {
                    System.err.println("Metric too high " + metric);
                    metric = 100;
                } else if (Double.isNaN(metric)) {
                    System.err.println("Metric not a number!");
                    metric = 0;
                    System.err.println(getMilestoneMetric());
                }
                interested = metric;
                return metric;
            } else {
                String SQL = "SELECT output FROM qprime WHERE key = ? AND mean = ? AND median = ? AND milestone = ? ORDER BY qprime DESC LIMIT 1;";
                PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
                statement.setString(1, getKey());
                statement.setDouble(2, QLearner.roundUpToInterval(getMeanMetric()));
                statement.setDouble(3, QLearner.roundUpToInterval(getMedianMetric()));
                statement.setDouble(4, QLearner.roundUpToInterval(getMilestoneMetric()));

                ResultSet resultSet = statement.executeQuery();
                if (!resultSet.next()) {
                    System.err.println("Nothing");
                }

                interested = resultSet.getDouble("output");
                return interested;
            }
        } else {
            return interested;
        }
    }

    /*package*/ double getMedianMetric() throws SQLException {
        String SQL = "SELECT median FROM summary WHERE id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, getKey());
        ResultSet results = statement.executeQuery();
        if (results.isBeforeFirst()) {
            results.next();
        } else {
            return 50;
        }
        double median = results.getDouble("median");

        if (median == 0) {
            return 50;
        }


        double ratio = coreValue / median;

        if (ratio < 1 && ratio != 0) {
            ratio = 1 / ratio;
        }

        return Math.min(ratio * 10, 100);
    }

    public double getCoreValue() {
        return coreValue;
    }

    protected abstract String getExplanation(double val);

    protected abstract double getIntrinsicMetric();

    public abstract String getKey();

    public abstract String getElementId();

    @Override
    public String toString() {
        return getExplanation(coreValue);
    }

    /*package*/ double getMilestoneMetric() {
        double log = Math.log10(coreValue);
        double roundedLog = Math.floor(log); // wrong way round on purpose!
        if (log == roundedLog) {
            return 100;
        }

        double metric = (log-roundedLog) * 100;

        if (log < 2) {
            metric *= 0.3;
        } else if (log < 4) {
            metric *= 0.5;
        }

        return metric;
    }

    /*package*/ double getMeanMetric() throws SQLException {
        String SQL = "SELECT mean FROM summary WHERE id = ?";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, getKey());
        ResultSet results = statement.executeQuery();
        if (results.isBeforeFirst()) {
            results.next();
        } else {
            return 50;
        }
        double mean = results.getDouble("mean");

        if (mean == 0) {
            return 50;
        }

        double ratio = coreValue / mean;

        if (ratio < 1 && ratio != 0) {
            ratio = 1 / ratio;
        }

        return Math.min(ratio * 10, 100);

    }

    public static void updateParametersFromDB() throws SQLException {
        String SQL = "SELECT * FROM params ORDER BY seq DESC;";
        ResultSet results = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        if (results.next()) {
            nextMilestoneAlpha = results.getDouble("milestone");
            meanAlpha = results.getDouble("mean");
            medianAlpha = results.getDouble("median");
            intrinsicAlpha = results.getDouble("intrinsic");
        }
    }

    private static void saveParameters(double mean, double median, double milestone, double intrinsic) throws SQLException {
        //normalize parameters
        double total = mean + median + milestone + intrinsic;
        double normmean = mean / total;
        double normmedian = median / total;
        double normmilestone = milestone / total;
        double normintrinsic = intrinsic / total;
        String SQL = "INSERT INTO params (mean, median, milestone, intrinsic) VALUES (?, ?, ?, ?);";
        PreparedStatement statement = DatabaseInitializer.getWritableConnection().prepareStatement(SQL);
        statement.setDouble(1, normmean);
        statement.setDouble(2, normmedian);
        statement.setDouble(3, normmilestone);
        statement.setDouble(4, normintrinsic);
        statement.executeUpdate();

        updateParametersFromDB();
    }

    public static void updateMeanAlpha(double newValue) throws SQLException {
        saveParameters(newValue, medianAlpha, nextMilestoneAlpha, intrinsicAlpha);
    }

    public static void updateMedianAlpha(double newValue) throws SQLException {
        saveParameters(meanAlpha, newValue, nextMilestoneAlpha, intrinsicAlpha);
    }

    public static void updateMilestoneAlpha(double newValue) throws SQLException {
        saveParameters(meanAlpha, medianAlpha, newValue, intrinsicAlpha);
    }

    public static void updateIntrinsicAlpha(double newValue) throws SQLException {
        saveParameters(meanAlpha, medianAlpha, nextMilestoneAlpha, newValue);
    }
}
