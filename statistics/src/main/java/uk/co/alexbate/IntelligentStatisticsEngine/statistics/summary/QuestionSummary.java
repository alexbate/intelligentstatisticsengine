package uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alex on 08/12/15.
 */
public class QuestionSummary extends Summary {

    static HashMap<String, String> keyToColMap = new HashMap<>();
    static {
        keyToColMap.put("question_views", "views");
        keyToColMap.put("correct_answers", "num_correct_answers");
        keyToColMap.put("incorrect_answers", "num_incorrect_answers");
        keyToColMap.put("question_hints", "hints_viewed");
    }

    @Override
    public void runSummaryTasks() throws SQLException {

        for (Map.Entry<String, String> e : keyToColMap.entrySet()) {
            double mean, median;
            if (!e.getValue().equals("hints_viewed")) {
                mean = calculateMean(e.getValue());
                median = calculateMedian(e.getValue());
            } else {
                mean = calculateHintMean();
                median = calculateHintMedian();
            }

            String SQL = "DELETE FROM summary WHERE id = ?; INSERT INTO summary (id, mean, median, mode, stdev) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            statement.setString(1, e.getKey());
            statement.setString(2, e.getKey());
            statement.setDouble(3, mean);
            statement.setDouble(4, median);
            statement.setDouble(5, 0);
            statement.setDouble(6, 0);
            statement.execute();
        }
    }

    private static double calculateMean(String col) throws SQLException {
        String SQL = "SELECT AVG(" + col +") FROM question";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getDouble("avg");
    }

    private static double calculateMedian(String col) throws SQLException {
        String SQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY "+col+") FROM question";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getDouble("percentile_cont");
    }

    private static double calculateHintMean() throws SQLException {
        String SQL = "SELECT AVG(sum) FROM question_hints_view";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getDouble("avg");
    }

    private static double calculateHintMedian() throws SQLException {
        String SQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY sum) FROM question_hints_view;";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getDouble("percentile_cont");
    }
}
