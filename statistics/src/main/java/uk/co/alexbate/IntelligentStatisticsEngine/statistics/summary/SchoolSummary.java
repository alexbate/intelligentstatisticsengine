package uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 11/12/15.
 */
public class SchoolSummary extends Summary {
    @Override
    public void runSummaryTasks() throws SQLException {
        runUsersTask();
    }

    private void runUsersTask() throws SQLException {
        String meanSQL = "SELECT AVG(count) FROM school_counts";
        ResultSet meanResults = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(meanSQL);
        meanResults.next();
        double mean = meanResults.getDouble("avg");

        String medianSQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY count) FROM school_counts";
        ResultSet medianResults = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(medianSQL);
        medianResults.next();
        double median = medianResults.getDouble("percentile_cont");

        String insertSQL = "DELETE FROM summary WHERE id = 'school_users'; INSERT INTO summary (id, mean, median) VALUES ('school_users', ?, ?);";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);
        statement.setDouble(1, mean);
        statement.setDouble(2, median);

        statement.execute();

    }
}
