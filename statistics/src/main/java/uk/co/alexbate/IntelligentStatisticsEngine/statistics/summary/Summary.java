package uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.MyRunnable;

import java.sql.SQLException;

/**
 * Created by alex on 01/12/15.
 */
public abstract class Summary extends MyRunnable {
    abstract void runSummaryTasks() throws SQLException;

    @Override
    public void run() {
        try {
            runSummaryTasks();
            this.taskComplete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
