package uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.getter.SummaryGetter;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats.Statistic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by alex on 09/12/15.
 */
public class SummarySummary extends Summary {
    SummaryGetter getter = new SummaryGetter();

    @Override
    public void runSummaryTasks() throws SQLException {
        mean();
    }

    private void mean() throws SQLException {
        List<Statistic> stats = getter.getStatistics();
        for (Statistic s : stats) {
            final String SQL = "SELECT * FROM summary WHERE summary.id = ?";
            PreparedStatement checkStatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
            checkStatement.setString(1, s.getKey());
            ResultSet results = checkStatement.executeQuery();
            double newMean;
            double numMeasurements;
            String insertSQL = "";
            boolean exists = false;
            if (results.isBeforeFirst()) {
                exists = true;
                results.next();
                double oldMean = results.getDouble("mean");

                //exploiting stdev
                numMeasurements = results.getDouble("stdev");

                newMean = ((oldMean * numMeasurements) + s.getCoreValue()) / (numMeasurements + 1);
                numMeasurements++;

                insertSQL += "DELETE FROM summary WHERE id = ?;";
            } else {
                newMean = s.getCoreValue();
                numMeasurements = 1;
            }

            insertSQL += "INSERT INTO summary (id, mean, stdev) VALUES (?, ?, ?);";
            PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);

            if (exists) {
                statement.setString(1, s.getKey());
                statement.setString(2, s.getKey());
                statement.setDouble(3, newMean);
                statement.setDouble(4, numMeasurements);
            } else {
                statement.setString(1, s.getKey());
                statement.setDouble(2, newMean);
                statement.setDouble(3, numMeasurements);
            }

            statement.execute();
        }
    }
}
