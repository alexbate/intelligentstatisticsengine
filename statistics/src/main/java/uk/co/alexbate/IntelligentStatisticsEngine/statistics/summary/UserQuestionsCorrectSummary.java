package uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 01/12/15.
 */
public class UserQuestionsCorrectSummary extends Summary {
    @Override
    public void runSummaryTasks() throws SQLException {
        double mean = calculateMean();
        double median = calculateMedian();

        String SQL = "DELETE FROM summary WHERE id = ?; INSERT INTO summary (id, mean, median, mode, stdev) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        statement.setString(1, "questions_correct");
        statement.setString(2, "questions_correct");
        statement.setDouble(3, mean);
        statement.setDouble(4, median);
        statement.setDouble(5, 0);
        statement.setDouble(6, 0);
        statement.execute();

        runViewsAndHintsTask();
        runLeaderboardTask();
    }

    private static double calculateMean() throws SQLException {
        String SQL = "SELECT AVG(questions_correct) FROM user_table";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getDouble("avg");
    }

    private static double calculateMedian() throws SQLException {
        String SQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY questions_correct) FROM user_table";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(SQL);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getDouble("percentile_cont");
    }

    private void runViewsAndHintsTask() throws SQLException {
        String meanSQL = "SELECT AVG(count), AVG(sum) AS hints FROM user_question_views";
        ResultSet meanResults = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(meanSQL);
        meanResults.next();
        double mean = meanResults.getDouble("avg");
        double hintsMean = meanResults.getDouble("hints");

        String medianSQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY count) FROM user_question_views";
        ResultSet medianResults = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(medianSQL);
        medianResults.next();
        double median = medianResults.getDouble("percentile_cont");

        String hintsMedianSQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY count) FROM user_question_views";
        ResultSet hintsMedianResults = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(medianSQL);
        hintsMedianResults.next();
        double hintsMedian = hintsMedianResults.getDouble("percentile_cont");

        String insertSQL = "DELETE FROM summary WHERE id = 'user_questions_viewed'; INSERT INTO summary (id, mean, median) VALUES ('user_questions_viewed', ?, ?);";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);
        statement.setDouble(1, mean);
        statement.setDouble(2, median);

        String hintinsertSQL = "DELETE FROM summary WHERE id = 'user_hints'; INSERT INTO summary (id, mean, median) VALUES ('user_hints', ?, ?);";
        PreparedStatement hintstatement = DatabaseInitializer.getDatabaseConnection().prepareStatement(hintinsertSQL);
        hintstatement.setDouble(1, hintsMean);
        hintstatement.setDouble(2, hintsMedian);

        hintstatement.execute();

    }

    private void runLeaderboardTask() throws SQLException {
        String numUsersSQL = "SELECT count(*) FROM user_table";
        ResultSet r = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(numUsersSQL);
        r.next();
        int users = r.getInt("count");
        double median = users / 2;
        String insertSQL = "DELETE FROM summary where id = 'leaderboard'; INSERT INTO summary (id, median) VALUES ('leaderboard', ?)";
        PreparedStatement statement = DatabaseInitializer.getDatabaseConnection().prepareStatement(insertSQL);
        statement.setDouble(1, median);
        statement.execute();
    }

    private void runHintsTask() throws SQLException {
    }
}
