package uk.co.alexbate.IntelligentStatisticsEngine.statistics.summary;

import uk.co.alexbate.IntelligentStatisticsEngine.statistics.DatabaseInitializer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by alex on 24/01/16.
 */
public class VideoSummary extends Summary {
    @Override
    void runSummaryTasks() throws SQLException {
        runMeanViewsTask();
    }

    private void runMeanViewsTask() throws SQLException {
        final String SQL = "SELECT AVG(play_frequency) FROM video_event WHERE time_in_seconds = 0";
        ResultSet resultSet = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        resultSet.next();
        double mean = resultSet.getDouble("avg");

        final String medianSQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY play_frequency) FROM video_event WHERE time_in_seconds = 0;";
        resultSet = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(medianSQL);
        resultSet.next();
        double median = resultSet.getDouble("percentile_cont");

        final String updateSQL = "DELETE from summary WHERE id = ?; INSERT INTO summary (id, mean, median) VALUES (?, ?, ?);";
        PreparedStatement statement = DatabaseInitializer.getWritableConnection().prepareStatement(updateSQL);
        statement.setString(1, "video_views");
        statement.setString(2, "video_views");
        statement.setDouble(3, mean);
        statement.setDouble(4, median);
        statement.execute();
    }

    private void runMostPausesTask() throws SQLException {
        final String SQL = "SELECT AVG(pause_frequency) FROM video_event WHERE time_in_seconds <> 0";
        ResultSet resultSet = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(SQL);
        resultSet.next();
        double mean = resultSet.getDouble("avg");

        final String medianSQL = "SELECT percentile_cont(0.50) WITHIN GROUP (ORDER BY pause_frequency) FROM video_event WHERE time_in_seconds <> 0;";
        resultSet = DatabaseInitializer.getDatabaseConnection().createStatement().executeQuery(medianSQL);
        resultSet.next();
        double median = resultSet.getDouble("percentile_cont");

        final String updateSQL = "DELETE from summary WHERE id = ?; INSERT INTO summary (id, mean, median) VALUES (?, ?, ?);";
        PreparedStatement statement = DatabaseInitializer.getWritableConnection().prepareStatement(updateSQL);
        statement.setString(1, "video_most_pauses");
        statement.setString(2, "video_most_pauses");
        statement.setDouble(3, mean);
        statement.setDouble(4, median);
        statement.execute();
    }
}
