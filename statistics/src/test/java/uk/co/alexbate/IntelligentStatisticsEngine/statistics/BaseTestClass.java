package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import org.junit.Before;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by alex on 08/11/15.
 */
public class BaseTestClass {
    @Before
    public void clearDatabase() {
        Configuration.DATABASE_NAME="isaac_stats_test";

        Connection dbConnection = DatabaseInitializer.getDatabaseConnection();

        final String sql = "DROP TABLE IF EXISTS question, question_tag, shares, tags, user_question, user_table, users, video_event, question_answer";

        try {
            Statement statement = dbConnection.createStatement();
            statement.execute(sql);
            statement.close();
            dbConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
