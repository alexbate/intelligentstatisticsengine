package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by alex on 08/11/15.
 */
public class DatabaseInitializerTest extends BaseTestClass {

    @Test
    public void testGetDatabaseConnection_returnsNotNull() throws Exception {
        assertNotNull(DatabaseInitializer.getDatabaseConnection());
    }
}