package uk.co.alexbate.IntelligentStatisticsEngine.statistics;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * HelloTest
 * Created by alex on 08/11/15.
 */
public class HelloTest extends BaseTestClass {

    Hello unitUnderTest;

    @Before
    public void setup() {
        unitUnderTest = new Hello();
    }

    @Test
    public void testHelloWorld() throws Exception {
        assertEquals(unitUnderTest.helloWorld(), "Hello world!");
    }
}