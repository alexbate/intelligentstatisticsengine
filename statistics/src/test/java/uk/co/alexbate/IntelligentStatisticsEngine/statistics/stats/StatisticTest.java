package uk.co.alexbate.IntelligentStatisticsEngine.statistics.stats;

import org.junit.Before;
import org.junit.Test;
import uk.co.alexbate.IntelligentStatisticsEngine.statistics.Configuration;

import static org.junit.Assert.*;

/**
 * Created by alex on 01/12/15.
 */
public class StatisticTest {

    private final double val = 100.0;

    Statistic unitUnderTest;

    @Before
    public void setUp() throws Exception {
        unitUnderTest = new Statistic(val) {
            @Override
            protected String getExplanation(double val) {
                return "Test statistic with value " + val;
            }

            @Override
            protected double getIntrinsicMetric() {
                return 0;
            }

            @Override
            public String getKey() {
                return "test";
            }

            @Override
            public String getElementId() {
                return "testID";
            }
        };
    }

    @Test
    public void testGetCoreValue() {
        assertEquals(val, unitUnderTest.getCoreValue(), 0.0);
    }

    @Test
    public void testGetInterestedMetric() throws Exception {
        Configuration.useQLearning = false;
        assertNotEquals(0.0, unitUnderTest.getInterestedMetric());
    }

    @Test
    public void testGetMilestoneMetric() throws Exception {
        assertEquals(100.0, unitUnderTest.getMilestoneMetric(), 0);
    }
}