## User Study Tool

### Installation
1. `./gradlew userStudyTool:run`

### Overview 
This is a small module which makes requests to multiple instances of the statistics module, and displays the results via HTTP. It records all HTTP rendered for analysis.
