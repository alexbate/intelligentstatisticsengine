package uk.co.alexbate.IntelligentStatisticsEngine;


import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static spark.Spark.get;
import static spark.Spark.port;

/**
 * Created by alex on 09/02/16.
 */
public class userStudyTool {
    public enum statisticType {
        question {
            @Override
            public List<String> getItems() {
                LinkedList<String> l =  new LinkedList<>();
                l.add("a2_ft_upper_36_num");
                l.add("a2_ft_core_6");
                l.add("field_surface");
                l.add("flying_off_earth");
                l.add("mixture_of_gases");
                return l;
            }
        },

        user {
          @Override
            public List<String> getItems() {
              LinkedList<String> l =  new LinkedList<>();
              l.add("8337");
              l.add("8496");
              l.add("10121");
              l.add("7667");
              l.add("5576");
              return l;
          }
        },
        video {
            @Override
            public List<String> getItems() {
                LinkedList<String> l =  new LinkedList<>();
                l.add("avLcUZqMu8I");
                l.add("gOdr4kUDUrw");
                l.add("t9_tSvHG2HI");
                l.add("gmd2wMfpnbo");
                l.add("6i_jVUxtV-U");
                return l;
            }
        },
        school {
            @Override
            public List<String> getItems() {
                LinkedList<String> l =  new LinkedList<>();
                l.add("128383");
                l.add("136642");
                l.add("117511");
                l.add("100003");
                l.add("109334");
                return l;
            }
        }, summary{
            @Override
            public List<String> getItems() {
                LinkedList<String> l =  new LinkedList<>();
                l.add("128383");
                l.add("136642");
                l.add("117511");
                l.add("100003");
                l.add("109334");
                return l;
            }
        };

        public abstract List<String> getItems();
    }

    public static String hostName = "http://localhost";

    public static int[] ports = {8081, 8082, 8083};

    static Writer writer = openNewFile();

    public static void main(String[] args) {
        port(5000);

        get("/", ((request, response) -> {
            String output = "<a href=\"/part1\">Part 1 </a> <br /><a href=\"/part2\">Part 2 </a>";
            writer.write(output);
            writer.flush();
            return output;
        }));

        get("/part1", ((request1, response1) -> {
            String output = "<html><body>";
            for (statisticType t : statisticType.values()) {
                if (!t.equals(statisticType.summary)) {
                    output += "<a href=\"/choose/" + t.name() + "\">" + t.name() + "</a><br />";
                }
            }
            output += "<a href=\"/display/summary\">summary</a>";
            writer.write(output);
            writer.flush();
            return output + "</body></html>";
        }));

        get("/part2", ((request1, response1) -> {
            String output = "<html><body>";
            for (statisticType t : statisticType.values()) {
                if (!t.equals(statisticType.video) && !t.equals(statisticType.summary)) {
                    output += "<a href=\"/display/" + t.name() + "\">" + t.name() + "</a><br />";
                }
            }
            writer.write(output);
            writer.flush();
            return output + "</body></html>";
        }));

        get("/choose/:thing", (request, response) -> {
            String output = generateHTMLForChoosingPage(statisticType.valueOf(request.params(":thing")));
            writer.write(output);
            writer.flush();
            return output;
        });

        get("/display/:thing/:query", (request, response) -> {
            writer.write("\nChosen "+ request.params(":thing") + " " + request.params(":query") +"\n");
            String output = generateHTMLForPage(statisticType.valueOf(request.params(":thing")), request.params(":query"));
            writer.write(output);
            writer.flush();
            return output;
        });

        get("/display/:thing", (request, response) -> {
            writer.write("\nLooking at the best " + request.params(":thing") + "\n");
            String output = generateHTMLForPage(statisticType.valueOf(request.params(":thing")), "");
            writer.write(output);
            writer.flush();
            return output;
        });

    }

    private static Writer openNewFile() {
        Date d = new Date();
        String fileName = d.toString();

        try {
            Files.createFile(Paths.get("output/" + fileName));
            return Files.newBufferedWriter(Paths.get("output/" + fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String generateHTMLForPage(statisticType type, String query) throws UnirestException {
        final String header = "<html><head><script src=\"//code.jquery.com/jquery-1.12.0.min.js\"></script>\n" +
                "<script src=\"//code.jquery.com/jquery-migrate-1.2.1.min.js\"></script><script>function toggle() {$( \".metric\" ).toggle();}</script><body>";

        final String tableStart = "<button id=\"btn\" onclick=\"toggle()\">Show metrics</button><table style=\"width:100%; height: 100%;\">";

        String rows = "";

        for (int p : ports) {
            rows += generateTableRow(type, query, p);
        }

        final String tableEnd = "</table>";

        final String footer = "</body></html>";

        return header + tableStart + rows + tableEnd + footer;
    }

    private static String generateHTMLForChoosingPage(statisticType type) {
        final String header = "<html><head><script src=\"//code.jquery.com/jquery-1.12.0.min.js\"></script>\n" +
                "<script src=\"//code.jquery.com/jquery-migrate-1.2.1.min.js\"></script><script>$(document).ready(function() \n" +
                "{ \n" +
                "  $('select[name=query]').change(function(v)\n" +
                "  {\n" +
                "    window.location.replace( \"/display/" + type.toString() + "/\" + $(this).val() );\n" +
                "  });\n" +
                "});</script></head><body>";

        final String pageHeader = "<h1>Choose a " + type.toString() + "</h1>";

        final String selectStart = "<select name=\"query\">";

        String options = "<option></option>";

        for (String s : type.getItems()) {
            options += "<option value=\"" + s + "\">" + s + "</option>";
        }

        final String selectEnd = "</select>";

        final String footer = "</body></html>";

        return header + pageHeader + selectStart + options + selectEnd + footer;
    }

    private static String generateTableRow(statisticType type, String query, int port) throws UnirestException {
        final String rowStart = "<tr>";
        String url = hostName + ":" + Integer.toString(port) + "/" + type;
        if (!query.equals("")) {
            url = url + "/" + query;
        }

        String s = Unirest.get(url).asString().getBody();
        JSONObject json = new JSONObject(s);

        String cells = "";

        for (Object key : json.keySet()) {
            String keyS = (String) key;
            JSONArray a = json.getJSONArray(keyS);
            JSONObject stat = a.getJSONObject(0);
            cells += generateCellFromStatJSON(stat);
        }

        final String rowEnd = "</tr>";

        return rowStart + cells + rowEnd;
    }

    private static String generateCellFromStatJSON(JSONObject stat) {
        final String cellStart = "<td>";
        final String mean;
        final String text = stat.getString("explanation");
        final String bumper = "<br />";
        final String thing = "Item represented: " + stat.getString("elementId");
        if (stat.has("mean")) {
            mean = "Mean: " + stat.getDouble("mean");
        } else{
            mean = "";
        }
        final String metric = "<div class=\"metric\" style=\"display:none\">Interested metric: " + stat.getDouble("interestingMetric") +"</div>";

        final String cellEnd = "</td>";

        return cellStart + text + bumper + thing + bumper + mean + bumper + metric + cellEnd;
    }
}
